import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';


import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";
import GetRequest from '../component/GetRequest';
import CabRequest from '../component/CabRequest';
import ManagerRequest from '../component/ManagerRequest'


function TabPanel(props) {
  const { children, value, index, ...other } = props;
  console.log(props)

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};


const useStyles = makeStyles(theme => ({

  tab: {
    minWidth: 200,
    width: 200,
  },
  indicator: {
    backgroundColor: "grey",
    height: "3px",
    top: "40px"
  },
  tabsWrapper: {
    backgroundColor: "Orange",
    color: "black",

  }
}));


export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div >
      <BrowserRouter>
        <AppBar position="static">
          <Tabs value={value} onChange={handleChange}
            centered className={classes.tabsWrapper}
            classes={{
              indicator: classes.indicator
            }}

            aria-label="simple tabs example" >
            <Tab label="Request eCabs" component={Link} to="/request" />
            <Tab label="eCab Status" component={Link} to="/eCabStatus" />
            <Tab label="Approval Requests" component={Link} to="/manager" />

          </Tabs>

        </AppBar>
        <TabPanel>
          {window.location.pathname === "/tabs" && (<CabRequest />)}
        </TabPanel>
        <Switch>
          <Route exact path="/eCabStatus" component={GetRequest} />
          <Route exact path="/request" component={CabRequest} />
          <Route exact path="/manager" component={ManagerRequest} />

        </Switch>
      </BrowserRouter>

    </div>
  );
}