import React, { Component } from 'react';
import { Route, Switch} from 'react-router';
import Login from './component/Login';
import TabRoutes from './component/TabRoutes';
import adminView from './component/adminView';
import VendorList from './component/VendorList';
import AddNewVendor from './component/AddNewVendor';
import EditVendor from './component/EditVendor';

export default class App extends Component {
  displayName = App.name
  

  render() 
  {
    return(  
     <div className="root">
        <Switch>       
        <Route exact path="/" component={Login} />         
        <Route exact path="/adminView" component={adminView} />
        <Route exact path="/VendorList" component={VendorList} />
        <Route exact path="/tabs" component={TabRoutes} />    
        <Route exact path="/AddNewVendor" component={AddNewVendor} />
        <Route exact path="/EditVendor" component={EditVendor} />
        </Switch>
        
      </div>
    );
  }
}
