import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import EditVendor from './EditVendor.js';
import { withRouter } from 'react-router';
import { appConfig } from '../Configuration/config.js';
import axios from 'axios';
import swal from '@sweetalert/with-react';
import '../App.css';

import {
    Link
  } from 'react-router-dom'

class EditDeleteButton extends React.Component {

    baseURL = appConfig.vendorDeleteApi;

    constructor(props) {
        super(props);
    }

    // componentDidMount() {
    //     this.props.history.push('/VendorList')
    // }
    delete(data)
    {
        console.log("delete" ,data.vendor_id)

        const resp = axios
        .delete(this.baseURL + data.vendor_id)
        .then(response => {
            // console.log(response)
            // console.log(response.data.message)
            swal({
                title: "Done!",
                text: "Vendor details Deleted successfully for " + data.vehicle_number +" !!",
                icon: "success",
                button: "OK",
            })
            this.onCloseModal();   
            this.props.context.componentParent.getVendorListDetails();
            // this.props.forceUpdate();
            this.props.history.push('/VendorList')
        })
        .catch(error => {
            console.log(error)
        })


    }

    onCloseModal = () => {
        this.setState({ open: false });
        this.refresh();
      };
      refresh()
      {
        this.setState({text:true})
      }
    render() {
        // console.log("editdeletebutton props", this.props.data)

        return (
            <div>
                <Button variant="primary" onClick={() =>
                    this.props.history.push('/EditVendor', this.props.data
                    )} style={styleUpdate}> <h6>Update</h6></Button>
                &nbsp;&nbsp;&nbsp;
                <Button variant="danger" onClick={() => {if(window.confirm('Are you sure to delete this record?')) this.delete(this.props.data)}}
                    style={styleUpdate}><h6>Delete</h6></Button>
            </div>
        );
    }
}

const styleUpdate = {
    color: "white",
    textAlign: "center",
    textDecorationLine: 'underline',
    fontStyle: 'italic',
}

export default withRouter(EditDeleteButton);
