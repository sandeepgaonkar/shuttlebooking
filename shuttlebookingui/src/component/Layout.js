import React, { Component } from 'react';
import  NavMenu  from './NavMenu';
import Navbar from 'react-bootstrap/Navbar'

export class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
      <div>
        <NavMenu />        
      </div>
    );
  }
}
