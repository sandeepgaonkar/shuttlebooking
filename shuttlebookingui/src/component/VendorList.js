import React from 'react';
import axios from 'axios';
import { AgGridReact } from 'ag-grid-react';
import { Button } from 'react-bootstrap';
import '../App.css';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { appConfig } from '../Configuration/config.js';
import { Layout } from './Layout';
import EditDeleteButton from './EditDeleteButton.js'
import AddNewVendor from './AddNewVendor.js';

class VendorList extends React.Component {
  
  baseURL = appConfig.vendorApi;
  constructor(props) {
    super(props);
    this.state = {
      selectedRows: '',
      columnDefs: [
        { headerName: 'Actions', cellRenderer: 'editDeleteButton', width: 200 ,
         colId: 'params' },
        // { headerName: 'Id', field: 'vendor_id', sortable: 'true', filter: 'true', minWidth: 30, maxWidth: 70 },
        { headerName: 'Vehicle Number', field: 'vehicle_number', sortable: 'true', filter: 'agTextColumnFilter', Width: 50 },
        { headerName: 'Vendor Name', field: 'vendor_name', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Vendor Contact', field: 'vendor_contact', sortable: 'true', filter: 'true', Width: 15 },
        { headerName: 'Vendor Address', field: 'vendor_address', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Vendor Email', field: 'vendor_email', sortable: 'true', filter: 'true' },
        { headerName: 'Vehicle Model', field: 'vehicle_model', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Owner Name', field: 'owner_name', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Owner Contact', field: 'owner_contact', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Driver Name', field: 'driver_name', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Driver Contact', field: 'driver_contact', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'License No', field: 'license_number', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Insurance Expiry Date', field: 'insurance_valid_till', sortable: 'true', filter: 'true', Width: 50 },
        { headerName: 'Seat Capacity', field: 'no_of_seats', sortable: 'true', filter: 'true', Width: 50 }
      ],
      requestList: [],
      onSelectionChanged: this.onSelectionChanged,
      frameworkComponents: {
        editDeleteButton: EditDeleteButton,

      },
      gridOptions: {
        getRowNodeId: function (data) {
          return data.id;
        }
      }
    };
  } 

  getVendorListDetails = () => {
    axios.get(this.baseURL)
      .then(res => {
        const items = res.data.data[0];
        console.log("Cabs Registered List", items)
        this.setState({ requestList: items })
        return this.item;
      })
  }

  onFirstDataRendered = params => {
    params.api.sizeColumnsToFit();
  };


  componentDidMount() {
    var requestList = this.getVendorListDetails();
    console.log("Admin page Cab List :", requestList);

  }

  render() {
    return (
      <div id="grid" className="ag-theme-alpine">
        <Layout />
        <br />
        <div>
          <Button variant="success" onClick={() =>
            this.props.history.push('/AddNewVendor')}>
            <h5 >Click Here to Add New Vendor</h5></Button>
          <br></br>
          <br></br>
        </div>

        <AgGridReact
          containerStyle={{
            height: '400px'
          }}
          columnDefs={this.state.columnDefs}
          rowData={this.state.requestList}
          rowSelection="single"
          onSelectionChanged={this.state.onSelectionChanged}
          context={this.state.context}
          frameworkComponents={this.state.frameworkComponents}
          pagination='true'
          paginationPageSize='20'
          enableColResize='true'
          sortable='true'
          filter='true'
          singleClickEdit='true'
          stopEditingWhenGridLosesFocus='true'
        />
      </div>
    )
  }
}

export default VendorList;