import React, { Component } from 'react';
import axios from 'axios';
import { Modal } from "react-responsive-modal";
import "react-responsive-modal/styles.css";
import './admin.css';
import swal from 'sweetalert';
import Button from 'react-bootstrap/Button'
import TransportRequestData from './TransportRequestData';
import { myConfig } from './Config.js'

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

export default class ChildMessageRenderer extends Component {
  baseURL = myConfig.apiUrl;
  
  constructor(props) {
    super(props);
    this.state = {
        Showmodal : false,
        disabled : true,
        requestList : [],
        open: false,
        vehicalList : [],
        text : false,
        requestId : '',
        status : '',
        vendor_id : '',
        driver_id : '',
        employee_id : '',
        cab_status : props.value,
        errormsg:'',
        isValid:true
      };
    
    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
    this.RequestData = this.RequestData.bind(this);
    this.getVehicalData = this.getVehicalData.bind(this);
  } 

  handleChange = (event) => {
    this.setState({vendor_id : event.target.value});
  };

  onOpenModal = () => {
    this.setState({ open: true });
    this.setState({requestId : TransportRequestData.getRequestId()})
  };

  onCloseModal = () => {
    this.setState({ open: false });
    this.refresh();
  };
  refresh()
  {
    this.setState({text:true})
  }
 
  RequestData(){
   
    var req_id = TransportRequestData.getRequestId();
    this.setState({requestId : req_id});

    console.log("Selected Request ID : ", req_id)
    console.log("Selected Request ID State : ", this.requestId)
   
     var driver_id = TransportRequestData.getVendorId();
     this.setState({driver_id : driver_id});

     console.log("Selected Vendor : ", driver_id)
     console.log("Selected Vendor state: ", this.driver_id)
          
  }

  getVehicalData()
  {
    this.RequestData(); 
    axios.get(this.baseURL+"Vendor")
      .then(res => {
         const items = res.data.data[0];
         this.setState({vehicalList : items}) 
         console.log("Vehical List : ", items)       
         console.log("Vehical List state: ", this.state.vehicalList)  
       }) 
  }
 
  async componentDidMount()
  {
    this.getVehicalData();  
  } 

  getTransportRequest()
    {
    axios.get(this.baseURL+"transportrequest/TransportRequestByReqId/"+TransportRequestData.getRequestId())
    .then(res => {
      const items = res.data.data[0];
      console.log("Transport Request Items : ", items);
    })
    }
  submit = async (event) => {

    var vendorId = this.state.vendor_id ===''? TransportRequestData.getVendorId(): this.state.vendor_id;
    axios.post(this.baseURL + "managerequest/AllocateShuttle", {          
    request_id : document.getElementById('requestId').value,
    vendor_id : vendorId        
    })  
    
  .then(response => {
    console.log("Response Data :", response.data);
    var result = response.data.data[0];
    console.log("Vehical List : ", result);    
    this.getTransportRequest();   
      swal({
      title: 'Cab Assigned Successfully!'      
    })   
    this.onCloseModal();   
    this.props.context.componentParent.getTransportRequestDetails();
    })   
  }

  render() {   
        
    const { open } = this.state; 
    var check = this.state.vehical  
    var cab = this.state.cab_status > 0 ? "Assigned" : "Unassigned"
    var btn_color = cab === "Unassigned" ? "status-unassigned" : "status-btn"
    var vendorId = TransportRequestData.getVendorId() === 0? "Select Cab" : TransportRequestData.getVehicalNumber();
    let options = this.state.vehicalList.map((data) =>
    <option 
    key={data.vendor_id}
    value={data.vendor_id} >
    {data.vehicle_number}                     
    </option>
    );               
       
     return (
       <div style={styles}>            
        <Button size="sm" className={btn_color}
         onClick={this.onOpenModal} >
        {cab}</Button>
        <div></div>
         <Modal open={open} onClose={this.onCloseModal}> 
         <span className="header">       
         Available Cabs
         </span>
         <form class="form-inline">      
         <span>Request Id</span>          
         <div >          
         <input type="text" id = "requestId" className="txt-box" value = {TransportRequestData.getRequestId()} disabled name="requestid" 
          />
          </div>
          <div >     
          <span>Cab</span>  
          <br/> 
          <select id = "vendorId" className="txt-box" onChange={this.handleChange} required>
          <option>{vendorId}</option>
          {options}
          <div style={{color: "red"}}>{this.state.errormsg}</div>
          </select>  
          </div> 
          <br/>
          <center>
          <button
          className = "assign-cab-btn"  onClick= {this.submit}>    
           Assign Cab        
          </button>           
          </center>                    
          </form> 
       </Modal>      
       </div> 
     );
   }
  }

