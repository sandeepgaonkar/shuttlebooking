import React, { Component } from "react";
import {myConfig} from './Config.js'
import userProfile from './UserProfile';



export default class GetRequest extends Component {
  baseURL = myConfig.apiUrl;

  constructor(props) {
    super(props);
    this.state = {
     loading: true,
     data: ''
  };
  }

  
async componentDidMount() {
 const url = this.baseURL + 'transportrequest' +'/'+ userProfile.getEmployeeID();
const response = await fetch(url);
const data = await  response.json();
this.setState({data:data.data[0],loading: false})
console.log(data)

}

  render() {
    return ( 
      <div> 
            {this.state.loading || !this.state.data ?(
                <div> loading ...</div>
            ) : (
                <div>
                <div>Request Time-&nbsp;&nbsp;&nbsp;{this.state.data.request_time}</div>
                <div>Status-&nbsp;&nbsp;&nbsp;{this.state.data.status_name}</div>

                    </div>
            )}
              </div>
       
    );
  }
}

