import React, { Component } from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Swal from 'sweetalert2'
import userProfile from './UserProfile';
import { myConfig } from './Config.js'
import axios from 'axios';
import './admin.css'

export default class ManagerRequest extends Component {
  baseURL = myConfig.apiUrl;
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      manageRequests: [],
      status: ''

    };

  }


  handleClick = (id) => {
    console.log(id)
    Swal.fire({
      title: 'Request Approval',
      input: 'select',
      inputOptions: this.state.status.slice(1).map((data) => {
        return (
          data.status_name


        )
      }),
      showCancelButton: true,
    }).then(function (result) {
      if (result.value) {
        const baseURL = myConfig.apiUrl;
        const resp = axios.post(baseURL + "managerequest/ApproveByManager", {
          "approved_by": userProfile.getManagerShortName(),
          "request_id": id,
          "status_id": (++result.value + 1)
        }
        )

          .then(
            response => {

              console.log("Response Data :", response.data);
              var result = response.data.data;

              console.log('post status', result)
            }
          )
      }
    }).then(() => {
      this.getManageRequests()
      this.getManageRequests()
    }
    )
  }



  async getStatus() {
    const url = this.baseURL + "status"
    const response1 = await fetch(url);
    const data = await response1.json();
    this.setState({ status: data.data[0], loading: false })
    console.log("status", this.state.status)
  }

  async getManageRequests() {
    const url = this.baseURL + "ManageRequest/GetByManagerId" + "/" + userProfile.getEmployeeID();
    const response = await fetch(url);
    const data = await response.json();
    this.setState({ manageRequests: data.data, loading: false })
    console.log(this.state.manageRequests)

  }

  async componentDidMount() {
    this.getManageRequests()
    this.getStatus()

  }

  render() {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="center" >EmpId</TableCell>
            <TableCell align="center" >Employee Name</TableCell>
            <TableCell align="center" >Contact </TableCell>
            <TableCell align="right" >Request Reason</TableCell>
            <TableCell align="center" >Drop Location</TableCell>
            <TableCell align="center" >Request Time</TableCell>
            <TableCell align="center" >Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {
            this.state.manageRequests.map((manageRequest, index) => {
              return <TableRow key={index}  >

                <TableCell align="center" >{manageRequest.employee_id}</TableCell>
                <TableCell align="center" >{manageRequest.employee_name}</TableCell>
                <TableCell align="center" >{manageRequest.contact_phone}</TableCell>
                <TableCell align="center" >{manageRequest.request_reason}</TableCell>
                <TableCell align="center" >{manageRequest.to_location}</TableCell>
                <TableCell align="center" >{manageRequest.request_time}</TableCell>
                <TableCell align="center" onClick={() => this.handleClick(manageRequest.request_id)} ><button class="approval-request-btn" >{manageRequest.status_name}</button></TableCell>

              </TableRow>
            })
          }
        </TableBody>
      </Table>
    );
  }
}