import React from 'react';
import axios from 'axios';
import logo from '../images/icons/emids.png';
import { appConfig } from '../Configuration/config.js';
import '../Login.css'
import '../css/main.css';
import userProfile from './UserProfile';
import { Button } from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

export default class Login extends React.Component {
  baseUrl = appConfig.loginApi;
  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        username: '',
        password: '',
        error: '',
        isValid: ''
      },
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });

  }

  onSubmit = async (event) => {
    
    const { username, password } = this.state;
    if((username === "admin" || username === "Admin" ) && (password ===  'admin123')){
      this.props.history.push('/adminview')
    }
    const resp = await axios.get(this.baseUrl + username + "&password=" + password)
      .then(response => {
        console.log("Response Data :", response.data);
        var result = response.data;
        console.log("Result2:", result);
        userProfile.setEmailAddress(response.data.emailAddress);
        userProfile.setEmployeeID(response.data.employeeID);
        userProfile.setEmployeeName(response.data.employeeName);
        userProfile.setProject(response.data.project)
        userProfile.setGender(response.data.gender);
        userProfile.setPrimaryPhoneNumber(response.data.primaryPhoneNumber);
        userProfile.setEmergencyPhoneNumber(response.data.emergencyPhoneNumber);
        userProfile.setEmailAddress(response.data.emailAddress);
        userProfile.setEmployeeShortName(response.data.employeeShortName);
        userProfile.setAccount(response.data.account);
        userProfile.setPracticeArea(response.data.practiceArea);
        userProfile.setManagerID(response.data.managerID);
        userProfile.setManagerName(response.data.managerName);
        userProfile.setManagerEmailAddress(response.data.managerEmailAddress);
        userProfile.setManagerShortName(response.data.managerShortName);
        userProfile.setManagerPrimaryPhoneNumber(response.data.managerPrimaryPhoneNumber);
        userProfile.setManagerEmergencyPhoneNumber(response.data.managerEmergencyPhoneNumber);
        userProfile.setManagerManagerID(response.data.managerManagerID);
        userProfile.setManagerManagerName(response.data.managerManagerName);
        userProfile.setManagerManagerShortName(response.data.managerManagerShortName);
        userProfile.setManagerManagerEmailAddress(response.data.managerManagerEmailAddress);
        userProfile.setManagerManagerPrimaryPhoneNumber(response.data.managerManagerPrimaryPhoneNumber);
        userProfile.setManagerManagerEmergencyPhoneNumber(response.data.managerManagerEmergencyPhoneNumber);
        userProfile.setResult(response.data.result);
        if (response.data.result === true) {
          this.props.history.push('/tabs');
        }

        else if (response.data.result === false) {
          this.setState({ error: "Your Username/Password is wrong", isValid: false })
        }
      })
    this.resetFormFields();

  }

  resetFormFields = () => {
    this.setState({
      username: '',
      password: ''
    });
  }

  render() {
    var check = this.state.isValid ? null : this.state.error;
    const { username, password } = this.state
    return (
      <div>
        <center>
        </center>
        <div class="limiter">
          <div class="container-page">
            <div class="wrap-login">
              <ValidatorForm  onSubmit={this.onSubmit} noValidate autoComplete="off" onCancel={this.onCancel} onError={errors => console.log(errors)}
              >
                <span class="login100-form-title p-b-26">
                  <img src={logo} alt="icon" width={100} height={35} />
                </span>
                <span class="login100-form-title p-b-26">
                  eCab Services
          </span>
                <TextValidator
                  id="username"
                  name="username"
                  type="text"
                  placeholder="Username"
                  margin="normal"
                  onChange={this.onChange}
                  value={username}
                  onInput={(e) => {
                    let regex = /^[a-zA-Z]/;
                    if(e.target.value.match(regex)){
                      this.setState({username:e.target.value,isValid:true})    }
                    else(
                      this.setState({error:"Please enter valid Username",isValid:false})
                      
                    )
                  }}
                 
                  className="wrap-input50"
                  validators={['required']}
                  errorMessages={['Enter the Username']}
                />

                <TextValidator
         
                  id="password"
                  name="password"
                  placeholder="Password"
                  type="password"
                  margin="normal"
                  value={password}
                  onChange={this.onChange}
                  className="wrap-input50"
                  validators={['required']}
                  errorMessages={['Enter the Password']}
                />
                <div class="container-page-form-btn">
                  <div class="wrap-page-form-btn">
                    <div class="page-form-bgbtn" />
                    <Button
                      type="submit"
                      class="page-form-btn wrap-upper"
                      
                
                    >
                      <i class="fa fa-sign-out" aria-hidden="true"></i>
                      Login            
                      
                    </Button>
                  </div>
                </div>
                <div style={{ color: "red" }}>{check}</div>
              </ValidatorForm>
            </div>
          </div>
        </div>
      </div>

    );
  }
}


