import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Layout } from './Layout';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import { appConfig } from '../Configuration/config.js';
import swal from '@sweetalert/with-react';
import { fontSize } from '@material-ui/system';

class AddNewVendor extends React.Component {

    baseURL = appConfig.vendorApi;

    constructor(props) {
        super(props);
        this.state = {
          inputs: {
            vendor_id: '',
            vehicle_id: '',
            vendor_name: '',
            vendor_contact: '',
            vendor_address: '',
            vendor_email: '',
            vehicle_number: '',
            vehicle_model: '',
            owner_name:'',
            owner_contact:'',
            driver_name:'',
            driver_contact:'',
            license_number:'',
            insurance_valid:'',
            insurance_valid_till:'',
            status:'1',
            no_of_seats:'',
            error: '',
            isValid: true
          },
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
      }

  
    onSubmit = async (event) => {
        event.preventDefault()
        const { vendor_name, vendor_contact, vendor_address, vendor_email, vehicle_number, vehicle_model,
            owner_name, owner_contact, driver_name, driver_contact, license_number, insurance_valid,
            insurance_valid_till, status, no_of_seats } = this.state;
        console.log(this.state)
        const resp = await axios
            .post(this.baseURL, this.state)
            .then(response => {
                console.log(response)
                console.log(response.data.message)
                swal({
                    title: "Done!",
                    text: response.data.message,
                    icon: "success",
                    button: "OK",
                });
                this.resetFormFields();
            })
            .catch(error => {
                console.log(error)
            })
    }

    resetFormFields = () => {
        this.setState({
            vendor_id: '',
            vehicle_id: '',
            vendor_name: '',
            vendor_contact: '',
            vendor_address: '',
            vendor_email: '',
            vehicle_number: '',
            vehicle_model: '',
            owner_name:'',
            owner_contact:'',
            driver_name:'',
            driver_contact:'',
            license_number:'',
            insurance_valid:'',
            insurance_valid_till:'',
            status:'',
            no_of_seats:'',
        });
      }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });

  } 

    render() {
        var check = this.state.isValid ? null : this.state.error;
        const {vendor_name,
        vendor_contact,
        vendor_address,
        vendor_email,
        vehicle_number,
        vehicle_model,
        owner_name,
        owner_contact,
        driver_name,
        driver_contact,
        license_number,
        insurance_valid,
        insurance_valid_till,
        status,
        no_of_seats} = this.state;

        return (


            <div id="grid" className="ag-theme-alpine">
                <Layout />
                <br />
                <h4 style={styleTitle}>eCabs - Shuttle Vendor Registration Page</h4>
                <br /><br />
                <h5 style={styleHeading}>Enter the Details for Registration</h5>
                <ValidatorForm style={{ textAlign: "center"}}
                    onSubmit={this.onSubmit} 
                    noValidate 
                    autoComplete="off" 
                    onCancel={this.onCancel} 
                    onError={errors => console.log(errors)}
                >
                    <TextValidator
                        id="vendor_name"
                        name="vendor_name"
                        label="Vendor Name"
                        margin="normal"
                        size="small"
                        value={vendor_name}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter vendor name']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="vendor_contact"
                        name="vendor_contact"
                        label="Vendor Contact"
                        margin="normal"
                        type="number"
                        size="small"
                        value={vendor_contact}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter vendor contact']}
                    />
                    <br></br>
                    <TextValidator
                        id="vendor_address"
                        name="vendor_address"
                        label="Vendor Address"
                        margin="normal"
                        size="small"
                        value={vendor_address}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter vendor address']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="vendor_email"
                        name="vendor_email"
                        label="Vendor Email"
                        margin="normal"
                        size="small"
                        value={vendor_email}
                        onChange={this.onChange}
                        validators={['required', 'isEmail']}
                        errorMessages={['this field is required', 'email is not valid']}
                        // validators={['required']}
                        // errorMessages={['Enter vendor email']}
                    />
                    <br></br>
                    <TextValidator
                        id="vehicle_number"
                        name="vehicle_number"
                        label="Vehicle Number"
                        margin="normal"
                        size="small"
                        value={vehicle_number}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter vehicle number']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="vehicle_model"
                        name="vehicle_model"
                        label="Vehicle Model"
                        margin="normal"
                        size="small"
                        value={vehicle_model}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter vehicle model']}
                    />
                    <br></br>

                    <TextValidator
                        id="owner_name"
                        name="owner_name"
                        label="Owner Name"
                        margin="normal"
                        size="small"
                        value={owner_name}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter owner name']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="owner_contact"
                        name="owner_contact"
                        label="Owner Contact"
                        margin="normal"
                        type="number"
                        size="small"
                        value={owner_contact}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter owner contact']}
                    />
                    <br></br>
                    
                    <TextValidator
                        id="driver_name"
                        name="driver_name"
                        label="Driver Name"
                        margin="normal"
                        size="small"
                        value={driver_name}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter driver name']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="driver_contact"
                        name="driver_contact"
                        label="Driver Contact"
                        margin="normal"
                        type="number"
                        size="small"
                        value={driver_contact}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter driver contact']}
                    />
                    <br></br>
                    <TextValidator
                        id="license_number"
                        name="license_number"
                        label="License Name"
                        margin="normal"
                        size="small"
                        value={license_number}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter license name']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="insurance_valid"
                        name="insurance_valid"
                        label="Insurance Valid"
                        margin="normal"
                        type="number"
                        size="small"
                        value={insurance_valid}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter insurance valid']}
                    />
                    <br></br>
                    <TextValidator
                        id="insurance_valid_till"
                        name="insurance_valid_till"
                        label="Insurance Valid Date"
                        type='date'
                        defaultValue='2000-12-31'
                        margin="normal"
                        size="small"
                        value={insurance_valid_till}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter license validity date']}
                    />
                     <br></br>
                    <TextValidator
                        id="status"
                        name="status"
                        label="Status"
                        margin="normal"
                        type="number"
                        size="small"
                        value={status}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter status']}
                    />
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <TextValidator
                        id="no_of_seats"
                        name="no_of_seats"
                        label="Seat Capacity"
                        margin="normal"
                        type="number"
                        size="small"
                        value={no_of_seats}
                        onChange={this.onChange}
                        validators={['required']}
                        errorMessages={['Enter seat capacity']}
                    />
                    <br></br>
                    <Button color="primary"
                    variant="contained" type="submit">SUBMIT</Button>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <Button color="primary"
                    variant="contained" type="submit" onClick={() =>
                        this.props.history.push('/VendorList')}>BACK</Button>
                    <br></br>
                    <div style={{ color: "red" }}>{check}</div>

                    </ValidatorForm>
            </div>

        );
    }
}

const styleTitle = {
    color: "orangered ",
    textAlign: "center",
    textDecorationLine: 'underline',

    // paddingTop: "100px",
}
const styleHeading = {
    color: "black",
    textAlign: "center",
    textDecorationLine: 'underline',
    fontStyle: 'italic',

    // paddingTop: "100px",
}
AddNewVendor.propTypes = {

};

export default withRouter(AddNewVendor)
