import React from 'react';
import {Button} from 'react-bootstrap';
import $ from 'jquery';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import '../index.css';
import '../images/icons/favicon.ico';          
import '../vendor/bootstrap/css/bootstrap.min.css';
import '../fonts/font-awesome-4.7.0/css/font-awesome.min.css';
import '../fonts/iconic/css/material-design-iconic-font.min.css';
import'../vendor/animate/animate.css';
import '../vendor/css-hamburgers/hamburgers.min.css';
import '../vendor/animsition/css/animsition.min.css';
import  '../vendor/select2/select2.min.css';
import '../vendor/daterangepicker/daterangepicker.css';
import '../css/main.css';
//import '../js/main.js';
import logo from '../images/icons/emids.png';


export default class demoLogin extends React.Component {

    constructor(props)
    {
     super(props);  
     this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this);
     this.state={
     username:'',        
     password:''
    } 
    
  } 
  handleSuccessfulAuth(data) {
    this.props.handleLogin(data);
    this.props.history.push("/dashboard");
  }
  
   
 componentDidMount()
  { 
   
  var username=document.getElementById("username").value;  
  var password=document.getElementById("password").value;
  var url = "http://127.0.0.1:5000/api/Values/login?userName="+username+"&password="+password;
  axios.get(url)
  .then(response => {
    console.log(response.data);
    if(response.data === false)
    {
      
    }
    
  })
  .catch(error => {
    console.log(error);
  });
  
  } 
     
      render() {
        return (
          <div className="demoLogin">
            <title>Shuttle Booking Login</title>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />  
            <noscript>You need to enable JavaScript to run this app.</noscript>
        
        <script type="text/javascript" src="static/js/main.316f1156.js"></script>       
            <style dangerouslySetInnerHTML={{__html: "\n" }} />
            <div className="limiter">
              <div className="container-page">
                <div className="wrap-login">
                  <form >
                    <span className="login100-form-title p-b-26">
                      <img src={logo} alt="icon" width={100} height={35} />
                    </span>
                    <span className="login100-form-title p-b-26">
                      eCab Services
                    </span>
                    <div className="wrap-input100 validate-input" data-validate="Enter username">
                      <input className="input100" type="text" name="username" id="username" />
                      <span className="focus-input100" data-placeholder="Username" />
                    </div>
                    <div className="wrap-input100 validate-input" data-validate="Enter password">
                      <span className="btn-show-pass">
                        <i className="zmdi zmdi-eye" />
                      </span>
                      <input className="input100" type="password" name="pass" id="password" />
                      <span className="focus-input100" data-placeholder="Password" />
                    </div>
                    <div className="container-page-form-btn">
                      <div className="wrap-page-form-btn">
                        <div className="page-form-bgbtn" />
                        <button className="page-form-btn wrap-upper" onClick={this.componentDidMount}>
                          Login
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
               
              </div>
            </div>            
      </div>        
      );
    }
  }
      
  
