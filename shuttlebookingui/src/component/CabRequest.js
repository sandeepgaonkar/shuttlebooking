import React from 'react';
import { Button, Select, FormControl, MenuItem, Input, InputLabel } from '@material-ui/core';
import axios from 'axios';
import swal from '@sweetalert/with-react';
import { withRouter } from 'react-router-dom';
import { myConfig } from './Config.js';
import Login from './Login.js';
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import '../css/Location.css'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import userProfile from './UserProfile';

class CabRequest extends React.Component {
  baseURL = myConfig.apiUrl;

  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        request_reason: '',
        contact_phone: '',
        request_time: '',
        pickup_location_id: '',
        to_location: '',
        project_account: '',
        to_lattitude: '',
        to_longitude: '',
        pickupLocation: [],
        error: '',
        isValid: true
      },
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }


  async componentDidMount(e) {
    this.setState({
      contact_phone: userProfile.getPrimaryPhoneNumber(),
      project_account: userProfile.getProject()
    })
    const url = this.baseURL + "Pickuplocation";
    const response = await fetch(url);
    const data = await response.json();
    this.setState({ pickupLocation: data.data[0], loading: false })
    console.log(this.state.pickupLocation)
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });

  }


  handleChange = to_location => {
    this.setState({ to_location });
  };

  handleSelect = to_location => {
    this.setState({ to_location });
    geocodeByAddress(to_location)
      .then(results => getLatLng(results[0]))
      .then(results => console.log('Lattitude : ', results.lat,
        this.setState({ to_lattitude: results.lat, to_longitude: results.lng })
      ))

      .then(latLng => console.log('Success', latLng))
      .catch(error => console.error('Error', error));
  };

  onSubmit = async (event) => {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds(); event.preventDefault();
    const { request_reason, contact_phone, request_time, pickup_location_id, to_location, project_account, to_lattitude, to_longitude } = this.state;
    const resp = await axios.post(this.baseURL + "transportrequest", {
      request_reason, request_time, pickup_location_id, to_location, project_account, to_lattitude, to_longitude, contact_phone,
      "employee_id": userProfile.getEmployeeID(),
      "first_name": userProfile.getEmployeeName(),
      "last_name": "GOOGLE",
      "user_name": "FB",
      "employee_name": userProfile.getEmployeeName(),
      "gender": userProfile.getGender(),
      "employee_username": userProfile.getEmployeeShortName(),
      "sex": userProfile.getGender(),
      "status": 1,
      "request_date": date + ' ' + time,
      "email": userProfile.getEmailAddress(),
      "pin_code": "560100",
      "no_of_passengers": 1,
      "statusname": "Request Pending Approval",
      "created_on": date + ' ' + time,
      "cancelled_on": "2013-06-01 00:00:00",
      "vendor_id": 0,
      "approved_by": "ADMIN1",
      "approved_on": "2013-06-01 00:00:00",
      "approved_by_dm": "DM1",
      "approved_by_dm_on": "10/19/2004 00:00:00",
      "approval_reason": "APPROVED",
      "total_expense": "0.00",
      "phone_no": userProfile.getPrimaryPhoneNumber(),
      "emergency_contact": userProfile.getEmergencyPhoneNumber(),
      "employeeShortName": userProfile.getEmployeeShortName(),
      "practice_area": userProfile.getPracticeArea(),
      "manager_id": userProfile.getManagerID(),
      "manager_username": userProfile.getManagerName(),
      "manager_email": userProfile.getManagerEmailAddress(),
      "manager_name": userProfile.getManagerShortName(),
      "manager_phone_no": userProfile.getManagerPrimaryPhoneNumber(),
      "manager_emergency_contact": userProfile.getManagerEmergencyPhoneNumber(),
      "manager_manager_id": userProfile.getManagerManagerID(),
      "manager_manager_name": userProfile.getManagerManagerName(),
      "manager_manager_username": userProfile.getManagerManagerShortName(),
      "manager_manager_phone_no": userProfile.getPrimaryPhoneNumber(),
      "manager_manager_email": userProfile.getManagerManagerEmailAddress(),
      "manager_manager_emergency_contact": userProfile.getManagerManagerEmergencyPhoneNumber(),
      "result": userProfile.getResult(),
    })
      .then(response => {
        console.log("Response Data :", response.data);
        var result = response.data.data;

        console.log('post status', result)
        swal("Your eCab request has been submitted for  ", ((this).state.request_time));
        this.resetFormFields();
      })
  }

  resetFormFields = () => {
    this.setState({
      request_reason: '',
      contact_phone: '',
      request_time: '',
      to_location: '',
      pickupLocation: null,
      project_account: '',
    });
  }

  render() {
    var check = this.state.isValid ? null : this.state.error;
    const { request_reason, contact_phone, request_time, pickup_location_id, to_location, to_lattitude, to_longitude, project_account } = this.state;
    return (


      <ValidatorForm
        onSubmit={this.onSubmit} noValidate autoComplete="off" onCancel={this.onCancel} onError={errors => console.log(errors)}
      >
        <TextValidator
          id="request_reason"
          name="request_reason"
          placeholder="Reason for Request"
          margin="normal"
          value={request_reason || ''}
          onChange={this.onChange}
          className="wrap-input50-ecab"
          validators={['required']}
          errorMessages={['Enter reason for eCab request']}
        />
        <br></br>
        <TextValidator
          id="contact_phone"
          name="contact_phone"
          type="number"
          placeholder="Mobile Number"
          margin="normal"
          onChange={this.onChange}
          value={contact_phone || ''}
          className="wrap-input50-ecab"
          onInput={(e) => {
            let regex = /^[0-9]/;
            if ((e.target.value.match(regex)) && (e.target.value.length < 10)) {
              this.setState({ error: "Please enter 10 digit mobile number", isValid: false })


            }
            else
              if ((e.target.value.match(regex)) && (e.target.value.length >= 10)) {
                this.setState({ contact_phone: e.target.value, isValid: true })

              }
            e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, 10)
          }}
          validators={['required']}
          errorMessages={['Enter 10 digit mobile number']}
        />
        <br></br>
        <TextValidator
          id="project_account"
          name="project_account"
          type="text"
          placeholder="Project Name"
          margin="normal"
          onChange={this.onChange}
          value={project_account || ''}
          className="wrap-input50-ecab"
          validators={['required']}
          errorMessages={['Enter the Project Name']}
        />
        <br></br>
        <TextValidator
          id="request_time"
          name="request_time"
          label="Pickup Time"
          type="time"
          margin="normal"
          value={request_time || ''}
          onChange={this.onChange}
          className="wrap-input50-ecab"
          validators={['required']}
          errorMessages={['Provide pickup time']}
        />


        <FormControl className="wrap-input50-ecab">
          <InputLabel htmlFor="pickup_location_id">Pickup Location</InputLabel>
          <Select
            onChange={this.onChange}
            input={<Input name="pickup_location_id" id="pickup_location_id" />}
          >
            {this.state.pickupLocation && this.state.pickupLocation.map(d => {
              return (
                <MenuItem key={d.pickup_location_id.toString()} value={d.pickup_location_id || ''}> {d.location}</MenuItem>
              )
            })}

          </Select>
        </FormControl>

        <div >
          <PlacesAutocomplete
            value={this.state.to_location || ''}
            onChange={this.handleChange}
            onSelect={this.handleSelect}
          >
            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
              <div>

                <TextValidator
                  {...getInputProps({
                    placeholder: 'Drop Location',
                    className: 'location-search-input'
                  })}

                  margin="normal"
                  className="wrap-input50-ecab"
                  runat="server"
                  validators={['required']}
                  errorMessages={['Enter the drop Location']}
                />

                <div className="autocomplete-dropdown-container">
                  {loading && <div>Loading...</div>}
                  {suggestions.map(suggestion => {

                    return (
                      <div className="input-suggestion"
                        {...getSuggestionItemProps(suggestion, {

                        })}
                      >
                        <i class="material-icons"> location_on </i> <span>{suggestion.description}</span>
                      </div>
                    );
                  })}
                </div>
              </div>
            )}
          </PlacesAutocomplete>
        </div>
        <div className="container-page-form-btn">
          <div className="wrap-page-form-btn-ecab">
            <div className="page-form-bgbtn" />
            <Button
              type="submit"
              className="wrap-page-form-btn wrap-upper"
            >
              Request for eCab
            </Button>
          </div>
        </div>
        <div style={{ color: "red" }}>{check}</div>
      </ValidatorForm>

    );
  }
}

export default withRouter(CabRequest)
