import React from 'react';
import SimpleTabs from '../tabs/simpleTab'
import emids from '../images/icons/emids.png';
import userProfile from './UserProfile';
import { Button } from '@material-ui/core';
import {FaUserAlt} from 'react-icons/fa';


class TabRoutes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        username: '',
        password: '',
        error: '',
        isValid: ''
      },
    };

    this.onClickSignOut = this.onClickSignOut.bind(this)
  }

  onClickSignOut() {
    this.props.history.push('/')
  }

  render() {
    return (

      <div class="limiter">
        <div class="container-page">
          <div class="wrap-page">
            <span class="login100-form-title p-b-26">
              <img src={emids} alt="icon" width="100" height="40" />
            </span>
            <span class="login100-form-title p-b-26">
            <h4 style={{marginTop: "10px"}}>Hi { userProfile.getEmployeeName() } </h4> 
          <h3 style={{marginTop: "8px"}}>Welcome to eCab Booking</h3> 

              <div className="wrap-page-form-btn-signout">
                <Button
                  type="submit"
                  onClick={this.onClickSignOut}
                >
                
               <FaUserAlt size="35px"/> 
            
           </Button>
              </div>
              <SimpleTabs />   </span>
       </div>
        </div>


      </div>
    )

  }
}
export default TabRoutes;