import React, { Component } from 'react';


var TransportRequestData = (function() {
   var request_id = "";
   var vehicalNumber = "";
   var driver_id = "";
   var requestList = [];
   var employee_id = "";
  
  var getRequestId = function() {
      return request_id;    // Or pull this from cookie/localStorage
    };
    

  var setRequestId = function(id) {
      request_id = id;     
      // Also set this in cookie/localStorage
    };

    var getVehicalNumber = function() {
      return vehicalNumber;    // Or pull this from cookie/localStorage
    };
    

  var setVehicalNumber = function(id) {
    vehicalNumber = id;     
      // Also set this in cookie/localStorage
    };
  
   var getVendorId = function()
   {
     return driver_id;
   } 

   var setVendorId = function(vid)
   {
     driver_id = vid
   }

   
  
  return {
    getRequestId: getRequestId,
    setRequestId: setRequestId,

    getVendorId : getVendorId,
    setVendorId : setVendorId,

    getVehicalNumber : getVehicalNumber,
    setVehicalNumber : setVehicalNumber   
  }

})();

export default TransportRequestData;


