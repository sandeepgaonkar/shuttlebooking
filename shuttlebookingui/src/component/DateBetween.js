import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import { myConfig } from './Config.js'
import './admin.css';

export default class DateBetween extends Component {
  baseURL = myConfig.apiUrl;
  state = {
    startDate: new Date(),
    endDate : new Date()
  }; 
 
  handleChange = date => {
    this.setState({
      startDate: date
    });
  }
    handleChange2 = date => {
      this.setState({
        endDate: date
      });
  }

  showdate=() => {
      console.log("Start Date : ", this.state.startDate.toISOString("dd/mm/yyyy"))
      console.log("End Date : ", this.state.endDate.toISOString().slice(0, 10).split('-').join('-').reverse())
      axios.post(this.baseURL + "GenerateReports", {          
        fromDate : this.state.startDate.toISOString().slice(0, 10).split('-').join('-'),
        toDate :this.state.endDate.toISOString().slice(0, 10).split('-').join('-')        
        }) 
        .then(response => {
          console.log("Response Data :", response.data);
          var result = response.data.data;
          console.log("Date Search Result : ", result);    })
  } 
  

  render() {
    const { value } = this.state;

    return (
      <div className="Sample">
        <tr>
          <td>
          <label>From Date :</label>
          </td>
          <td>
        <DatePicker className="input"        
        selected={this.state.startDate}
        onChange={this.handleChange}
        placeholderText = "Start Date"
        customStyles={{dateInput:{borderWidth: 1}}}
      />
      </td>
      <td>
      <label>To Date :</label>
      </td>
      <td>
        <DatePicker className="input"
        selected={this.state.endDate}
        onChange={this.handleChange2}
        placeholderText = "End Date"
      />
          </td>
          <td>
          <button onClick={this.showdate} className="search-btn"> search</button>
          </td>
        </tr>
        
      
      </div>
    );
  }
}