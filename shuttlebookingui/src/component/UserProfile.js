var UserProfile = (function () {
    var employeeID
    var employeeName = "";
    var gender = "";
    var primaryPhoneNumber = "";
    var emergencyPhoneNumber = "";
    var emailAddress = "";
    var employeeShortName = "";
    var account = "";
    var project ='';
    var practiceArea = "";
    var managerID = "";
    var managerName = "";
    var managerEmailAddress = "";
    var managerShortName = "";
    var managerPrimaryPhoneNumber = "";
    var managerEmergencyPhoneNumber = "";
    var managerManagerID = "";
    var managerManagerName = "";
    var managerManagerShortName = "";
    var managerManagerEmailAddress = "";
    var managerManagerPrimaryPhoneNumber = "";
    var managerManagerEmergencyPhoneNumber = "";
    var result = "";

    var setEmployeeName = function (employeeName1) {
        employeeName = employeeName1;
    };

    var getEmployeeName = function () {
        return employeeName;
    };


    var setEmployeeID = function (employeeID1) {
        employeeID = employeeID1;
    };
    var getEmployeeID = function () {
        return employeeID;
    };

    var setGender = function (gender1) {
        gender = gender1;
    };
    var getGender = function () {
        return gender;
    };

    var setProject = function (project1) {
        project = project1;
    };
    var getProject = function () {
        return project;
    };

    var setEmailAddress = function (emailAddress1) {
        emailAddress = emailAddress1;
    };
    var getEmailAddress = function () {
        return emailAddress;
    };

    var setPrimaryPhoneNumber = function (primaryPhoneNumber1) {
        primaryPhoneNumber = primaryPhoneNumber1;
    };
    var getPrimaryPhoneNumber = function () {
        return primaryPhoneNumber.slice(3,13);
    };

    var setEmergencyPhoneNumber = function (emergencyPhoneNumber1) {
        emergencyPhoneNumber = emergencyPhoneNumber1;
    };
    var getEmergencyPhoneNumber = function () {
        return emergencyPhoneNumber.slice(3,13);
    };

    var setEmployeeShortName = function (employeeShortName1) {
        employeeShortName = employeeShortName1;
    };
    var getEmployeeShortName = function () {
        return employeeShortName;
    };

    var setAccount = function (account1) {
        account = account1;
    };
    var getAccount = function () {
        return account;
    };

    var setPracticeArea = function (practiceArea1) {
        practiceArea = practiceArea1;
    };
    var getPracticeArea = function () {
        return practiceArea;
    };

    var setManagerID = function (managerID1) {
        managerID = managerID1;
    };
    var getManagerID = function () {
        return managerID;
    };

    var setManagerName = function (managerName1) {
        managerName = managerName1;
    };
    var getManagerName = function () {
        return managerName;
    };

    var setManagerEmailAddress = function (managerEmailAddress1) {
        managerEmailAddress = managerEmailAddress1;
    };
    var getManagerEmailAddress = function () {
        return managerEmailAddress;
    };


    var setManagerShortName = function (managerShortName1) {
        managerShortName = managerShortName1;
    };
    var getManagerShortName = function () {
        return managerShortName;
    };

    var setManagerPrimaryPhoneNumber = function (managerPrimaryPhoneNumber1) {
        managerPrimaryPhoneNumber = managerPrimaryPhoneNumber1;
    };
    var getManagerPrimaryPhoneNumber = function () {
        return managerPrimaryPhoneNumber.slice(3,13);
    };

    var setManagerEmergencyPhoneNumber = function (managerEmergencyPhoneNumber1) {
        managerEmergencyPhoneNumber = managerEmergencyPhoneNumber1;
    };
    var getManagerEmergencyPhoneNumber = function () {
        return managerEmergencyPhoneNumber;
    };

    var setManagerManagerID = function (managerManagerID1) {
        managerManagerID = managerManagerID1;
    };
    var getManagerManagerID = function () {
        return managerManagerID;
    };

    var setManagerManagerName = function (managerManagerName1) {
        managerManagerName = managerManagerName1;
    };
    var getManagerManagerName = function () {
        return managerManagerName;
    };


    var setManagerManagerShortName = function (managerManagerShortName1) {
        managerManagerShortName = managerManagerShortName1;
    };
    var getManagerManagerShortName = function () {
        return managerManagerShortName;
    };

    var setManagerManagerEmailAddress = function (managerManagerEmailAddress1) {
        managerManagerEmailAddress = managerManagerEmailAddress1;
    };
    var getManagerManagerEmailAddress = function () {
        return managerManagerEmailAddress;
    };

    var setManagerManagerPrimaryPhoneNumber = function (managerManagerPrimaryPhoneNumber1) {
        managerManagerPrimaryPhoneNumber = managerManagerPrimaryPhoneNumber1;
    };
    var getManagerManagerPrimaryPhoneNumber = function () {
        return managerManagerPrimaryPhoneNumber.slice(3,13);
    };

    var setManagerManagerEmergencyPhoneNumber = function (managerManagerEmergencyPhoneNumber1) {
        managerManagerEmergencyPhoneNumber = managerManagerEmergencyPhoneNumber1;
    };
    var getManagerManagerEmergencyPhoneNumber = function () {
        return managerManagerEmergencyPhoneNumber.slice(3,13);
    };
    var setResult = function (result) {
        result = result;
    }

    var getResult = function () {
        return result
    }


    return {
        getEmployeeName: getEmployeeName,
        setEmployeeName: setEmployeeName,
        setEmployeeID: setEmployeeID,
        getEmployeeID: getEmployeeID,
        setGender: setGender,
        getGender: getGender,
        setEmailAddress: setEmailAddress,
        getEmailAddress: getEmailAddress,
        setPrimaryPhoneNumber: setPrimaryPhoneNumber,
        getPrimaryPhoneNumber: getPrimaryPhoneNumber,
        setEmergencyPhoneNumber: setEmergencyPhoneNumber,
        getEmergencyPhoneNumber: getEmergencyPhoneNumber,
        setEmployeeShortName: setEmployeeShortName,
        getEmployeeShortName: getEmployeeShortName,
        setAccount: setAccount,
        getAccount: getAccount,
        setProject:setProject,
        getProject:getProject,
        setPracticeArea: setPracticeArea,
        getPracticeArea: getPracticeArea,
        setManagerID: setManagerID,
        getManagerID: getManagerID,
        setManagerName: setManagerName,
        getManagerName: getManagerName,
        setManagerEmailAddress: setManagerEmailAddress,
        getManagerEmailAddress: getManagerEmailAddress,
        setManagerShortName: setManagerShortName,
        getManagerShortName: getManagerShortName,
        setManagerPrimaryPhoneNumber: setManagerPrimaryPhoneNumber,
        getManagerPrimaryPhoneNumber: getManagerPrimaryPhoneNumber,
        setManagerEmergencyPhoneNumber: setManagerEmergencyPhoneNumber,
        getManagerEmergencyPhoneNumber: getManagerEmergencyPhoneNumber,
        setManagerManagerID: setManagerManagerID,
        getManagerManagerID: getManagerManagerID,
        setManagerManagerName: setManagerManagerName,
        getManagerManagerName: getManagerManagerName,
        setManagerManagerShortName: setManagerManagerShortName,
        getManagerManagerShortName: getManagerManagerShortName,
        setManagerManagerEmailAddress: setManagerManagerEmailAddress,
        getManagerManagerEmailAddress: getManagerManagerEmailAddress,
        setManagerManagerPrimaryPhoneNumber: setManagerManagerPrimaryPhoneNumber,
        getManagerManagerPrimaryPhoneNumber: getManagerManagerPrimaryPhoneNumber,
        setManagerManagerEmergencyPhoneNumber: setManagerManagerEmergencyPhoneNumber,
        getManagerManagerEmergencyPhoneNumber: getManagerManagerEmergencyPhoneNumber,
        setResult: setResult,
        getResult: getResult


















    }



})();

export default UserProfile;