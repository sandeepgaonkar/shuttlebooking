 import React from 'react';
  import { AgGridReact } from 'ag-grid-react';
  import '../App.css';
  import 'ag-grid-community/dist/styles/ag-grid.css';
  import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
  import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
  import ChildMessageRenderer from './ChildMessageRenderer';
  import { Layout } from './Layout';
  import TransportRequestData from './TransportRequestData';
  import axios from 'axios';
  import DatePicker from "react-datepicker"; 
  import "react-datepicker/dist/react-datepicker.css";
  import { myConfig } from './Config.js'
  import './admin.css';
  import styled, { css } from 'styled-components';



  export default class adminView extends React.Component {  
      baseUrl = myConfig.apiUrl;
      Wrapper = styled.div`
  &.react-datepicker {
    color: blue;
  }
`
      constructor(props) {
        super(props);
      this.state = {
        startDate: new Date(),
        endDate : new Date(),
        selectedDateRequestList : [],
        filter : false,
        error : '',
        selectedRows : '',
        modules: [ClientSideRowModelModule],
        columnDefs: [
          {headerName: 'Cab Status', field: 'vendor_id', cellRenderer: 'ChildMessageRenderer', colId: 'params', Width:80,  cellStyle: {textAlign: 'center'}
        },
        {headerName: 'Assigned'+' Cab', field: 'vehicle_number', sortable: 'true', filter:'true', minWidth: 100 },
          {headerName: 'Employee '+' Id', field: 'employee_id',sortable: 'true', filter:'true', minWidth:30},
          {headerName: 'Email', field: 'email', sortable: 'true', filter:'true', minWidth:50},
          { headerName: 'Request'+'' +' Date', field : 'request_date_time', sortable: 'true', filter:'true', minWidth: 150 },
          {headerName: ' Reason', field: 'request_reason', sortable: 'true', filter:'true', minWidth: 120},
          {headerName: 'Pickup'+' ' +'Location', field: 'pickup_location', minWidth: 90, sortable: 'true', filter:'true'},
          {headerName: 'Drop'+' ' +'Location', field: 'to_location', sortable: 'true', filter:'true', minWidth: 150},
          {headerName: 'Approved'+'' +' By', field: 'approval_reason',  sortable: 'true', filter:'true',minWidth: 120},
          {headerName: 'Approval'+'' +' Status', field: 'status_name', sortable: 'true', filter:'true', minWidth: 100 },  
                   
        ],
        requestList: [],
        context: { componentParent: this,
              },
          frameworkComponents: {      
            ChildMessageRenderer: ChildMessageRenderer,
          },  
          onSelectionChanged: this.onSelectionChanged, 
          gridOption : {
            getRowNodeId: function(data) {
              return data.id;
            }   
          }
      };
    }


    onSelectionChanged = (e) => {
      
      var selectedRows = e.api.getSelectedRows();
      var request_id =  selectedRows.length === 1 ? selectedRows[0].request_id : '';
      var vehicle_number = selectedRows.length === 1 ? selectedRows[0].vehicle_number : '';
      var vendor_id  = selectedRows.length === 1 ? selectedRows[0].vendor_id : '';
        
      console.log("Selected Request id = ", request_id)
      console.log("Selected vendor id = ", vendor_id)
      console.log("Selected employee = ", vehicle_number)
      

      TransportRequestData.setRequestId(request_id);  
      TransportRequestData.setVendorId(vendor_id);   
      TransportRequestData.setVehicalNumber(vehicle_number);    
    };

      methodFromParent = cell => {
        alert('Parent Component Method from ' + cell + '!');
      };  

    getTransportRequestDetails  = () => {
      axios.get(this.baseUrl+"transportrequest/")
      .then(res => {
        const items = res.data.data;
        console.log("AdminView Request List",items)  
        this.setState({requestList : items})   
        //TransportRequestData.setRequestList(items); 
        return this.item;
      })
    }

    onFirstDataRendered = params => {
      params.api.sizeColumnsToFit();
    };

    onGridSizeChanged = params => {
      var gridWidth = document.getElementById('grid').offsetWidth;
      var columnsToShow = [];
      var columnsToHide = [];
      var totalColsWidth = 0;
      var allColumns = params.columnApi.getAllColumns();
      for (var i = 0; i < allColumns.length; i++) {
        var column = allColumns[i];
        totalColsWidth += column.getMinWidth();
        if (totalColsWidth > gridWidth) {
          columnsToHide.push(column.colId);
        } else {
          columnsToShow.push(column.colId);
        }
      }
      params.columnApi.setColumnsVisible(columnsToShow, true);
      params.columnApi.setColumnsVisible(columnsToHide, false);
      params.api.sizeColumnsToFit();
    };


      componentDidMount() {
      var requestList = this.getTransportRequestDetails();
      console.log("Admin page request List :", requestList);     
      }

      handleChange = date => {
        this.setState({
          startDate: date
        });
      }
        handleChange2 = date => {
          this.setState({
            endDate: date
          });
      }
    
      showdate=() => {
        if((this.state.startDate === '' && this.state.endDate === '') || (this.state.startDate === '' || this.state.endDate === ''))
        {
          this.setState({error : "please select start & end date"})
        }
        else{
          console.log("Start Date : ", this.state.startDate.toISOString().slice(0,10));
          console.log("End Date : ", this.state.endDate.toISOString().slice(0, 10))
          axios.post( this.baseUrl+"GenerateReports",{          
            fromDate : this.state.startDate.toISOString().slice(0, 10).split('-').join('-'),
            toDate :this.state.endDate.toISOString().slice(0, 10).split('-').join('-')        
            }) 
          .then(response => {
            console.log("Response Data :", response.data);
            var result = response.data.data;
            this.setState({selectedDateRequestList : result, filter : true})
            console.log("Date Search Result : ", this.state.selectedDateRequestList);  
            })
          }
      }  

      reset()
      {
        //event.preventDefault();
        let start_date = new Date();
        let end_date = new Date();
        this.refs.start_date.setState({date: start_date
        }, () => {
            this.handleChange(start_date);
            this.handleChange2(end_date)
        });
        this.setState({filter : false})
        this.render();
      }
      
      render() {
      const { value } = this.state;
      var requestData = this.state.filter === false? this.state.requestList : this.state.selectedDateRequestList;
      return (  
        <div>
        <script src="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" >
       </script>
        <div id = "grid" className="ag-theme-alpine">
        <Layout/>  
        <br/>
        <center>
            <div >
            <tr>
            <td>
            <div className="date-container">
            <label>Start Date &nbsp;</label>
            <DatePicker         
            selected={this.state.startDate}
            onChange={this.handleChange}
            ref="start_date"
            placeholderText = "Start Date"
            input = {true}
            customStyles={{dateInput:{borderWidth: 1}}}
            isClearable={true}            
          />
          </div>
          </td>
          <td>
            <div className="date-container">
            <label>To Date &nbsp; </label>
            <DatePicker 
            selected={this.state.endDate}
            onChange={this.handleChange2}
            ref="end_date"
            input = {true}
            placeholderText = "End Date"
            isClearable={true}
            customStyles={{dateInput:{borderWidth: 1}}}
          />
          </div>
            </td>
            <td>
            <div className="button-container">
            <button onClick={this.showdate} className="search-btn"> search</button>
            <button className="reset-btn" onClick={() =>this.reset()}>reset</button>
            </div>
            </td>                      
          </tr>          
          </div>
          <span style={{color:"red"}}>{this.state.error}</span>
            </center>
            <br/>
            
            <AgGridReact             
            containerStyle={{       
            height: '462px'}}
            columnDefs={this.state.columnDefs}
            rowData={requestData}
            rowSelection="single"    
            onSelectionChanged={this.state.onSelectionChanged}
            context={this.state.context}
            frameworkComponents={this.state.frameworkComponents}
            pagination='true'
            paginationPageSize= '8'
            onFirstDataRendered={this.onFirstDataRendered.bind(this)}
            onGridSizeChanged={this.onGridSizeChanged.bind(this)}
            enableColResize = 'true'
            sortable= 'true'
            filter= 'true'
        />                     
          </div>
          </div>
        )
      }
    }