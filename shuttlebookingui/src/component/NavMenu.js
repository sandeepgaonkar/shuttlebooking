﻿import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Link } from "react-router-dom";
import logo from '../images/icons/emids.png';
import './NavMenu.css';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CenteredTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <div>  <center>
      <img src={logo} alt="icon" width={100} height={35} />
      <p className="heading">
        eCabs - Admin Portal
    </p>
    </center>
      {/* <nav class="navbar navbar-expand-sm bg-light navbar-light mb-0">
        <Button class="btn btn-warning btn-sm pull-right" href="" type="submit" >
          <i class="glyphicon glyphicon-user white" ></i>
          LOGOUT
        </Button>
      </nav> */}
      <Paper className={classes.root} >
        <Tabs
          value='false'
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="primary"
          selectionFollowsFocus='true'
          centered
        >
          <Tab label="Booking Requests" component={Link} to="/adminView" />
          <Tab label="Manage Vendors" component={Link} to="/VendorList" />
          <Tab disabled />
          <Tab disabled />
          <Tab label="Sign Out" className="signout-btn" component={Link} to="/" />
        </Tabs>

      </Paper>

    </div>

  );
}
