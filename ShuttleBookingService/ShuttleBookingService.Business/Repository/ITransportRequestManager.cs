﻿using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public interface ITransportRequestManager
    {
        Task<List<TransportRequest>> GetTransportRequests();
        Task<TransportRequest> AddTransportRequest(TransportRequest entity);
        Task<List<TransportRequest>> GetTransportRequestByManagerId(string ManagerId);
        Task<TransportRequest> GetTransportRequestById(int Id);

        Task<TransportRequest> GetTransportRequestByEmpId(string Id);
    }
}
