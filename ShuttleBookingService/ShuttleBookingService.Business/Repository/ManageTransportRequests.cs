﻿using Dapper;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class ManageTransportRequests : IManageTransportRequests
    {

        // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public ManageTransportRequests(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }

        public async Task<List<TransportRequest>> GetTransportRequestByManagerId(string managerId)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.GetTransportRequestByManagerId, new
                   {
                       @managerId = managerId,
                       @managerManagerId = managerId,
                   }));



                return _requestId.ToList();
            }



            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> ApproveRequestByManager(int RequestID, string ApproveBy, int StatusId, string Comment)
        {
            try
            {
                bool result = false;
                switch (StatusId)
                {
                    case 1:
                        result = true;
                        break;
                    case 2:
                        var _approvedRequest = await _executers.ExecuteCommand(
                                        conn => conn.QueryAsync<TransportRequest>(_commandText.ApproveByManager, new
                                        {
                                            @approved_by = ApproveBy,
                                            @requestId = RequestID,
                                            @satusid = StatusId,
                                            @approved_on = DateTime.Now,
                                            @comment = string.IsNullOrEmpty(Comment) ? "Approved by manager" : Comment
                                        })) ;
                        result = true;
                        break;
                    case 3:
                        var _cancelledRequest = await _executers.ExecuteCommand(
                                         conn => conn.QueryAsync<TransportRequest>(_commandText.CancelledByManager, new
                                         {
                                             @cancelled_by = ApproveBy,
                                             @requestId = RequestID,
                                             @satusid = StatusId,
                                             @cancelled_on = DateTime.Now,
                                             @comment = string.IsNullOrEmpty(Comment) ? "Cancelled by manager" : Comment
                                         }));
                        result = true;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        
        }

        public async Task<bool> AllocateShuttle(int RequestID, int VendorId)
        {
            try
            {
                    var _requestId = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.AllocateShuttle, new
                  {
                      @vendorId = VendorId,
                      @requestId = RequestID,
                  }));
                    return true;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
