﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace ShuttleBookingService.Business.Repository
{
    public class TransportRequestManager : ITransportRequestManager
    {
       // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public TransportRequestManager(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }
        public  async Task<List<TransportRequest>> GetTransportRequests()
        {
            try
            {
                AutoAproveTransportRequest();
                var query = await _executers.ExecuteCommand(
                  conn =>  conn.QueryAsync<TransportRequest>(_commandText.GetAllTransportRequests));
                return query.ToList();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TransportRequest> GetTransportRequestById(int Id)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.GetTransportRequestById, new
                   {
                       @request_id = Id
                   }));

                return _requestId.SingleOrDefault();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async void AutoAproveTransportRequest()
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.ApproveTransportRequest));                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<TransportRequest> GetTransportRequestByEmpId(string Id)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.GetTransportRequestByempId, new
                   {
                       @request_id = Id
                   }));

                return _requestId.SingleOrDefault();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<TransportRequest>> GetTransportRequestByManagerId(string managerId)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.GetTransportRequestById, new
                   {
                       @managerId = managerId,
                       @managerManagerId = managerId,
                   }));

                return _requestId.ToList();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<TransportRequest> AddTransportRequest(TransportRequest entity)
        {
            try
            {
                var employee = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.AddEmployeeDetails, new
                  {
                      employee_id = entity.employee_id,
                      employee_name = entity.employee_name,
                      employee_username = entity.employee_username,
                      gender = entity.gender,
                      phone_no = entity.contact_phone,
                      emergency_contact = entity.emergency_contact,
                      email = entity.email,
                      account = entity.project_account,
                      project = entity.project,
                      practice_area = entity.practice_area,
                      manager_id = entity.manager_id,
                      manager_name = entity.manager_name,
                      manager_username = entity.manager_username,
                      manager_email = entity.manager_email,
                      manager_phone_no = entity.manager_phone_no,
                      manager_emergency_contact = entity.manager_emergency_contact,
                      manager_manager_id = entity.manager_manager_id,
                      manager_manager_name = entity.manager_manager_name,
                      manager_manager_username = entity.manager_manager_username,
                      manager_manager_email = entity.manager_manager_email,
                      manager_manager_phone_no = entity.manager_manager_phone_no,
                      manager_manager_emergency_contact = entity.manager_manager_emergency_contact
                  }));

                var _requestId = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.AddTransportRequest, new
                  {
                      employee_id=entity.employee_id,
                      contact_phone=entity.contact_phone,
                      account=entity.account,
                      email=entity.email,
                      request_reason=entity.request_reason,
                      pickup_location_id=entity.pickup_location_id,
                      to_location=entity.to_location,
                      to_lattitude=entity.to_lattitude,
                      to_longitude=entity.to_longitude,
                      pin_code=entity.pin_code,
                      no_of_passengers=entity.no_of_passengers,
                      manager_id=entity.manager_id,
                      manager_manager_id=entity.manager_manager_id,
                      request_time=entity.request_time,
                      request_date=entity.request_date,
                  }));

                if(_requestId!=null)
                {
                    var currentRequest = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<TransportRequest>(_commandText.GetTransportRequestById, new
                   {
                       @request_id = Convert.ToInt32(_requestId.Single().request_id)
                   }));
                    return currentRequest.SingleOrDefault();
                }
                else
                {
                    return null;
                }

                
                //return _requestId.Single();

            }
            catch (Exception Ex)
            {
                throw;
            }
            
        }
    }
}
