﻿using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public interface IManageTransportRequests
    {
        //Task<List<TransportRequest>> GetTransportRequests();
        Task<bool> ApproveRequestByManager(int requestId, string ManagerId, int statusId, string comment);
        Task<List<TransportRequest>> GetTransportRequestByManagerId(string ManagerId);
        Task<bool> AllocateShuttle(int requestId, int VendorId);
    }
}
