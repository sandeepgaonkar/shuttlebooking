﻿using Dapper;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class VendorManager: IVendorManager
    {
        // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public VendorManager(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }
        public async Task<List<Vendor>> GetVendors()
        {
            try
            {
                var query = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<Vendor>(_commandText.GetAllVendors));
                return query.ToList();
                
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Vendor> VendorsRegistration(Vendor entity)
        {
            try
            {
                var _vendor = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<Vendor>(_commandText.VendorsRegistration, new
                  {
                      @vehicle_id=entity.vehicle_id,
                      @vendor_name=entity.vendor_name,
                      @vendor_contact=entity.vendor_contact,
                      @vendor_address=entity.vendor_address,
                      @vendor_email=entity.vendor_email,
                      @vehicle_number=entity.vehicle_number, 
                      @vehicle_model=entity.vehicle_model, 
                      @owner_name=entity.owner_name, 
                      @owner_contact=entity.owner_contact, 
                      @driver_name=entity.driver_name, 
                      @driver_contact=entity.driver_contact,
                      @license_number=entity.license_number, 
                      @insurance_valid=entity.insurance_valid,
                      @insurance_valid_till=entity.insurance_valid_till,
                      @status=entity.status, 
                      @no_of_seats=entity.no_of_seats
                  }));

                if (_vendor != null)
                {
                    return _vendor.SingleOrDefault();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception Ex)
            {
                throw;
            }

        }

        public async Task<Vendor> UpdateVendorById(Vendor entity)
        {
            try
            {
                
                var _vendor = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<Vendor>(_commandText.UpdateVendorById, new
                  {
                      @vendor_id = entity.vendor_id,
                      @vehicle_id = entity.vehicle_id,
                      @vendor_name = entity.vendor_name,
                      @vendor_contact = entity.vendor_contact,
                      @vendor_address = entity.vendor_address,
                      @vendor_email = entity.vendor_email,
                      @vehicle_number = entity.vehicle_number,
                      @vehicle_model = entity.vehicle_model,
                      @owner_name = entity.owner_name,
                      @owner_contact = entity.owner_contact,
                      @driver_name = entity.driver_name,
                      @driver_contact = entity.driver_contact,
                      @license_number = entity.license_number,
                      @insurance_valid = entity.insurance_valid,
                      @insurance_valid_till = entity.insurance_valid_till,
                      @status = entity.status,
                      @no_of_seats = entity.no_of_seats
                  }));

                if (_vendor != null)
                {
                    return _vendor.SingleOrDefault();
                }
                else
                {
                    return null;
                }


                //return _requestId.Single();

            }
            catch (Exception Ex)
            {
                throw;
            }

        }

        public async Task<Vendor> GetVendorsById(int Id)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<Vendor>(_commandText.GetVendorById, new
                   {
                       @vendor_id = Id
                   }));

                return _requestId.SingleOrDefault();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DeleteVendor(int Id)
        {
            try
            {
                var _requestId = await _executers.ExecuteCommand(
                   conn => conn.QueryAsync<Vendor>(_commandText.DeleteVendorById, new
                   {
                       @vendor_id = Id
                   }));
                if(_requestId.FirstOrDefault().vendor_id==Id && _requestId.FirstOrDefault().status=="0")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
