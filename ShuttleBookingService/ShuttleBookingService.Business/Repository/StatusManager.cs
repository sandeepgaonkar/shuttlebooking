﻿using Dapper;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class StatusManager : IStatusManager
    {
        // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public StatusManager(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }
        public async Task<List<Status>> GetStatus()
        {
            try
            {
                var query = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<Status>(_commandText.GetAllStatus));
                return query.ToList();

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
