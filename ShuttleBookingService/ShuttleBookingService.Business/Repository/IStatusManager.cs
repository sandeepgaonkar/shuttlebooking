﻿using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public interface IStatusManager
    {
        Task<List<Status>> GetStatus();
    }
}
