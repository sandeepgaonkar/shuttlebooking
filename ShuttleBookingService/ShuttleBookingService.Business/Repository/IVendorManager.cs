﻿using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public interface IVendorManager
    {
        Task<List<Vendor>> GetVendors();
        Task<Vendor> VendorsRegistration(Vendor vendors);
        Task<Vendor> GetVendorsById(int Id);
        Task<Vendor> UpdateVendorById(Vendor vendors);
         Task<bool> DeleteVendor(int Id);
    }
}
