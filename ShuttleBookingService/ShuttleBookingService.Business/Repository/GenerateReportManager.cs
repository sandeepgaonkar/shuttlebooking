﻿using Dapper;
using ShuttleBookingService.DataAccess.CommandExecuter;
using ShuttleBookingService.DataAccess.Queries;
using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Business.Repository
{
    public class GenerateReportManager : IGenerateReport
    {
        // private readonly IConfiguration _configuration;
        private readonly ICommandText _commandText;
        private readonly IExecuters _executers;
        public GenerateReportManager(ICommandText commandText, IExecuters executers)
        {
            _commandText = commandText;
            //_configuration = configuration;
            _executers = executers;
        }

        public async Task<List<TransportRequest>> GetRequestsByDate(DateTime fromDate, DateTime toDate, bool exportFlag)
        {
            try
            {
                //if(exportFlag)
                //{
                //    var query = await _executers.ExecuteCommand(
                //conn => conn.QueryAsync<TransportRequest>(_commandText.GenerateReportByDate, new
                //{
                //    @fromDate = fromDate.Date,
                //    @toDate = toDate.Date,
                //}));
                //    //DataTable dt = new DataTable();
                //    //dt.Columns.Add("Name");
                //    //dt.Columns.Add("Department");
                //    //dt.Columns.Add("IsManager");
                //    //dt.Rows.Add(query.ToArray().OrderByDescending(p => p.created_on).ToArray());

                   
                //        return query.ToList();
                //}
                //else
               // {
                    var query = await _executers.ExecuteCommand(
                  conn => conn.QueryAsync<TransportRequest>(_commandText.GenerateReportByDate,new {
                      @fromDate = fromDate.Date,
                      @toDate = toDate.Date,
                  }));
                    return query.ToList();
                //}
                
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
