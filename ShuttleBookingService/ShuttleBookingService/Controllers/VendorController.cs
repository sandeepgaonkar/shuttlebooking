﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;
using ShuttleBookingService.Model.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShuttleBookingService.Controllers
{
    
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class VendorController : Controller
    {
        private readonly IVendorManager _vendorManager;
        public VendorController(IVendorManager vendorManager)
        {
            _vendorManager = vendorManager;
        }
        // GET: api/<controller>
        [HttpGet]        
        public async Task<IActionResult> Get()
        {
            try
            {
                var vendorDetails = await _vendorManager.GetVendors();
                var apiResponse = new ApiResponse();
                if (vendorDetails != null)
                {
                    
                    apiResponse.message = "Find all vendors deatils";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(vendorDetails);
                    return Ok(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendors deatils not found";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }
        // GET api/<controller>/5
        [HttpGet("{vendorId}")]
        [Route("TransportRequestByReqId/{vendorId}")]
        public async Task<IActionResult> Get(int vendorId)
        {
            try
            {
                var vendorDetails = await _vendorManager.GetVendorsById(vendorId);
                var apiResponse = new ApiResponse();
                if (vendorDetails == null)
                {
                    apiResponse.message = "Vendor is not availble !!";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendor details for vendor id '" + vendorId + "'";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(vendorDetails);
                    return Ok(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex.Message);
            }
        }



        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Vendor requests)
        {
            try
            {
                var vendorDetails = await _vendorManager.VendorsRegistration(requests);
                var apiResponse = new ApiResponse();
                if (vendorDetails == null)
                {
                    apiResponse.message = "Vendor details is not added !!";
                    apiResponse.status = "Bad request";
                    return BadRequest(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendor details added successfully !!";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(vendorDetails);
                    return Ok(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex.Message);
            }
        }

        // PUT api/<controller>/5
        //[HttpPut("{id}")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]Vendor requests)
        {
            try
            {
                var vendorDetails = await _vendorManager.UpdateVendorById(requests);
                var apiResponse = new ApiResponse();
                if (vendorDetails == null)
                {
                    apiResponse.message = "Vendor is not availble !!";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendor details for vendor id '" + requests.vendor_id + "'";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(vendorDetails);
                    return Ok(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex.Message);
            }
        }

        // GET api/<controller>/5
        // DELETE api/<controller>/5
        [HttpDelete("{vendorId}")]
        [Route("TransportRequestByReqId/{vendorId}")]
        public async Task<IActionResult> Delete(int vendorId)
        {
            try
            {
                bool _vendorStatus = await _vendorManager.DeleteVendor(vendorId);
                var apiResponse = new ApiResponse();
                if (!_vendorStatus)
                {
                    apiResponse.message = "Vendor is not availble !!";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendor details deleted successfully for vendor id '" + vendorId + "'";
                    apiResponse.status = "Success";
                    return Ok(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex.Message);
            }
        }
    }
}
