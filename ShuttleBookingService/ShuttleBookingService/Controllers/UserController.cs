﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;

namespace ShuttleBookingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class UserController : ControllerBase
    {
        [HttpGet]
        [Route("login")]
        public async Task<UserInfo> GetAsync(string userName, string password)
        {
            HttpClient client = new HttpClient();
            // var byteArray = Encoding.ASCII.GetBytes("admin:Admin123");
            // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));          
            var baseURL = "http://arc-staging.emids.com:121/api/UserInfo?userName=";
            // var functionPath = "UserAuth?userName="+userName+"&password="+password;
            var functionPath = "" + userName + "&password=" + password;

            HttpResponseMessage response = await client.GetAsync(baseURL + functionPath);
            var result = await response.Content.ReadAsStringAsync();
            if (result != "false")
            {
                var resultResponse = JsonConvert.DeserializeObject<UserInfo>(result);
                resultResponse.result = true;
                return resultResponse;
                // return await response.Content.ReadAsStringAsync();
            }
            else
            {
                return new UserInfo() { result = false };
            }


        }

       
    }
    public class UserInfo{
            public string employeeID { get; set; }
        public bool result { get; set; }
       
        public string EmployeeName { get; set; }
        public string EmployeeShortName { get; set; }
        public string Gender { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string EmergencyPhoneNumber { get; set; }

        public string EmailAddress { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public string PracticeArea { get; set; }
        public string ManagerID { get; set; }
        public string ManagerName { get; set; }
        public string ManagerShortName { get; set; }
        public string ManagerEmailAddress { get; set; }
        public string ManagerPrimaryPhoneNumber { get; set; }
        public string ManagerEmergencyPhoneNumber { get; set; }
        public string ManagerManagerID { get; set; }
        public string ManagerManagerName { get; set; }
        public string ManagerManagerShortName { get; set; }
        public string ManagerManagerEmailAddress { get; set; }
        public string ManagerManagerPrimaryPhoneNumber { get; set; }
        public string ManagerManagerEmergencyPhoneNumber { get; set; }
       

    }
}
