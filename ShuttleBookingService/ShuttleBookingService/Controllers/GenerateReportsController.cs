﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using ShuttleBookingService.Business.Repository;

namespace ShuttleBookingService.Controllers
{
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class GenerateReportsController : Controller
    {
        private readonly IGenerateReport _generateReportManager;
        private readonly IHostingEnvironment _hostingEnvironment;
        public GenerateReportsController(IGenerateReport generateReportManager, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _generateReportManager = generateReportManager;
        }
        // POST api/<controller>

        [HttpGet]
        public FileResult Get()
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"demo.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Employee");
                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Name";
                worksheet.Cells[1, 3].Value = "Gender";
                worksheet.Cells[1, 4].Value = "Salary (in $)";

                //Add values
                worksheet.Cells["A2"].Value = 1000;
                worksheet.Cells["B2"].Value = "Jon";
                worksheet.Cells["C2"].Value = "M";
                worksheet.Cells["D2"].Value = 5000;

                worksheet.Cells["A3"].Value = 1001;
                worksheet.Cells["B3"].Value = "Graham";
                worksheet.Cells["C3"].Value = "M";
                worksheet.Cells["D3"].Value = 10000;

                worksheet.Cells["A4"].Value = 1002;
                worksheet.Cells["B4"].Value = "Jenny";
                worksheet.Cells["C4"].Value = "F";
                worksheet.Cells["D4"].Value = 5000;

                package.Save(); //Save the workbook.
            }
            var result = PhysicalFile(Path.Combine(sWebRootFolder, sFileName), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            Response.Headers["Content-Disposition"] = new ContentDispositionHeaderValue("attachment")
            {
                FileName = file.Name
            }.ToString();

            return result;
        }

        [HttpGet]
        [Route("{fromDate}/{toDate}")]
        public async Task<IActionResult> Get([FromBody] DateTime fromDate, DateTime toDate)
        {
            try
            {
                var requetsDetails = await _generateReportManager.GetRequestsByDate(fromDate.Date,toDate.Date,false);
                var apiResponse = new ApiResponse();
                if (requetsDetails == null)
                {
                    apiResponse.message = "Transport requests is not availble !!";
                    apiResponse.status = "Bad request";
                    return BadRequest(apiResponse);
                }
                else
                {
                    apiResponse.message = "Vendor details added successfully !!";
                    apiResponse.status = "Success";
                    foreach (var request in requetsDetails)
                    {
                        apiResponse.data.Add(new ShuttleBookingResponse
                        {
                            request_id = request.request_id,
                            employee_id = request.employee_id,
                            employee_name = request.employee_name,
                            contact_phone = request.contact_phone,
                            account = request.account,
                            email = request.email,
                            request_reason = request.request_reason,
                            pickup_location_id = request.pickup_location_id,
                            pickup_location = request.pickup_location,
                            to_location = request.to_location,
                            pin_code = request.pin_code,
                            no_of_passengers = request.no_of_passengers,
                            vendor_id = request.vendor_id,
                            vendor_name = request.vendor_name,
                            vehicle_model = request.vehicle_model,
                            vehicle_number = request.vehicle_number,
                            total_expense = request.total_expense,
                            manager_id = request.manager_id,
                            manager_manager_id = request.manager_manager_id,
                            approved_by = request.approved_by,
                            approved_on = request.approved_on,
                            request_time = request.request_time,
                            request_date = request.request_date,
                            request_date_time = String.Format("{0} {1}", request.request_date.Date.ToString("dd-MM-yyyy"), DateTime.Today.Add(request.request_time).ToString("hh:mm tt")),
                            created_on = request.created_on,
                            status_id = request.status_id,
                            status_name = request.status_name,
                            cancelled_by = request.cancelled_by,
                            cancelled_on = request.cancelled_on,
                            comment = request.comment,
                        });
                    }
                    
                      return Ok(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]GenerateReport requests)
        {
                      
            try
            {
                var requetsDetails = await _generateReportManager.GetRequestsByDate(requests.fromDate, requests.toDate, false);
                var apiResponse = new ApiResponse();
                if (requetsDetails == null)
                {
                    apiResponse.message = "Transport requests is not availble !!";
                    apiResponse.status = "Bad request";
                    return null;
                }
                else
                {
                    apiResponse.message = "Vendor details added successfully !!";
                    apiResponse.status = "Success";
                    foreach (var request in requetsDetails)
                    {
                        apiResponse.data.Add(new ShuttleBookingResponse
                        {
                            request_id = request.request_id,
                            employee_id = request.employee_id,
                            employee_name = request.employee_name,
                            contact_phone = request.contact_phone,
                            account = request.account,
                            email = request.email,
                            request_reason = request.request_reason,
                            pickup_location_id = request.pickup_location_id,
                            pickup_location = request.pickup_location,
                            to_location = request.to_location,
                            pin_code = request.pin_code,
                            no_of_passengers = request.no_of_passengers,
                            vendor_id = request.vendor_id,
                            vendor_name = request.vendor_name,
                            vehicle_model = request.vehicle_model,
                            vehicle_number = request.vehicle_number,
                            total_expense = request.total_expense,
                            manager_id = request.manager_id,
                            manager_manager_id = request.manager_manager_id,
                            approved_by = request.approved_by,
                            approved_on = request.approved_on,
                            request_time = request.request_time,
                            request_date = request.request_date,
                            request_date_time = String.Format("{0} {1}", request.request_date.Date.ToString("dd-MM-yyyy"), DateTime.Today.Add(request.request_time).ToString("hh:mm tt")),
                            created_on = request.created_on,
                            status_id = request.status_id,
                            status_name = request.status_name,
                            cancelled_by = request.cancelled_by,
                            cancelled_on = request.cancelled_on,
                            comment = request.comment,
                        });
                    }
                    /*if(requests.exportFlag)
                    {
                        DataTable table = new DataTable();
                        foreach (var value in apiResponse.data)
                        {
                            if (table.Columns.Count == 0)
                            {
                                foreach (var p in value.GetType().GetProperties())
                                {
                                    table.Columns.Add(p.Name);
                                }
                            }

                            DataRow dr = table.NewRow();
                            foreach (var p in value.GetType().GetProperties())
                            {
                                dr[p.Name] = p.GetValue(value, null) + "";

                            }
                            table.Rows.Add(dr);
                        }
                        string export = "export";
                        byte[] fileContents;
                        var stream = new MemoryStream();
                        using (var package = new ExcelPackage())
                        {
                            var worksheet = package.Workbook.Worksheets.Add(export);
                            worksheet.Cells["A1"].LoadFromDataTable(table, true);
                            fileContents = package.GetAsByteArray();
                            package.Save();
                        };
                        //if (fileContents == null || fileContents.Length == 0)
                        //{
                        //    return NotFound();
                        //}
                        // return File(package.GetAsByteArray(), "application/vnd.ms-excel", "Contact.xlsx");
                        //return File(
                        //    fileContents: fileContents,
                        //    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        //    fileDownloadName: export + ".xlsx"
                        //);
                        stream.Position = 0;
                        var contentType = "application/octet-stream";
                        var fileName = "fileName.xlsx";
                        return File(stream, contentType, fileName);
                        //return Ok(apiResponse);
                    }
                    else
                    {
                        return null;
                      //  return Ok(apiResponse);
                    }*/
                    return Ok(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return null;
                //return StatusCode(500, "Internal server error " + ex.Message);
            }
        }

        public void GenerateReport(List<object> data)
        {
            

            /*var firstRecord = data.First();
            if (firstRecord == null)
                return;
            var infos = data.GetType().GetProperties();
            DataTable table = new DataTable();
            foreach (var info in infos)
            {
                DataColumn column = new DataColumn(info.Name, info.PropertyType);
                table.Columns.Add(column);
            }

            foreach (var record in data)
            {
                DataRow row = table.NewRow();
                for (int i = 0; i < table.Columns.Count; i++)
                    row[i] = infos[i].GetValue(record);
                table.Rows.Add(row);
            }*/
        }
    }
}