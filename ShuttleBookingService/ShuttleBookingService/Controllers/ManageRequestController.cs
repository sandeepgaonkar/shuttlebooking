﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;
using ShuttleBookingService.Model.Model;

namespace ShuttleBookingService.Controllers
{
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]/[action]")]
    public class ManageRequestController : ControllerBase
    {
        private readonly IManageTransportRequests _manageTransportRequest;
        public ManageRequestController(IManageTransportRequests manageTransportRequest)
        {
            _manageTransportRequest = manageTransportRequest;
        }
        [HttpGet("{managerId}")]
        [ActionName("GetByManagerId")]
        public async Task<IActionResult> GetByManagerId(string managerId)
        {
            try
            {
                var requests = await _manageTransportRequest.GetTransportRequestByManagerId(managerId);
                var apiResponse = new ApiResponse();

                if (requests == null)
                {
                    apiResponse.message = "Shuttle booking requests not available !!";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                else
                {
                    apiResponse.message = "Shuttle booking request for managerId '" + managerId + "'";
                    apiResponse.status = "Success";
                    foreach (var request in requests)
                    {
                        apiResponse.data.Add(new ShuttleBookingResponse
                        {
                            request_id = request.request_id,
                            gender = request.gender,
                            employee_id = request.employee_id,
                            employee_name = request.employee_name,
                            contact_phone = request.contact_phone,
                            account = request.account,
                            email = request.email,
                            request_reason = request.request_reason,
                            pickup_location_id = request.pickup_location_id,
                            to_location = request.to_location,
                            pickup_location = request.pickup_location,
                            pin_code = request.pin_code,
                            no_of_passengers = request.no_of_passengers,
                            vendor_id = request.vendor_id,
                            total_expense = request.total_expense,
                            manager_id = request.manager_id,
                            manager_manager_id = request.manager_manager_id,
                            approved_by = request.approved_by,
                            approved_on = request.approved_on,
                            request_time = request.request_time,
                            request_date = request.request_date,
                            created_on = request.created_on,
                            status_id = request.status_id,
                            status_name = request.status_name,
                            cancelled_by = request.cancelled_by,
                            cancelled_on = request.cancelled_on,
                            comment = request.comment,
                        });

                    }
                    return Ok(apiResponse);
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

        
        [HttpPost]
       [ActionName("ApproveByManager")]
        public async Task<IActionResult> ApproveByManager([FromBody]ShuttleBookingResponse approveRequest)
        {
            
            try
            {
                var apiResponse = new ApiResponse();
                if (approveRequest.request_id>0 && !string.IsNullOrEmpty(approveRequest.approved_by) && approveRequest.status_id > 0)
                {
                    bool approveFlag = await _manageTransportRequest.ApproveRequestByManager(approveRequest.request_id, approveRequest.approved_by, approveRequest.status_id, approveRequest.comment);
                   
                    if (approveFlag)
                    {
                        apiResponse.message = "Shuttle booking status updated successfully !!";
                        apiResponse.status = "Success";
                        return Ok(apiResponse);
                    }
                    else
                    {
                        apiResponse.message = "Something went wrong !!";
                        apiResponse.status = "Failed";
                        return BadRequest(apiResponse);
                    }
                }

                else
                {
                    apiResponse.message = "Request parameter invalid !!";
                    apiResponse.status = "Failed";
                    return BadRequest(apiResponse);
                }
                
                
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

       // [HttpGet("{request_id}/{vendor_id}")]
        [HttpPost]
        //[ActionName("AllocateShuttle/requestId={requestId}&vendorId={vendorId}")]
        [ActionName("AllocateShuttle")]
        public async Task<IActionResult> AllocateShuttle([FromBody]ShuttleBookingResponse allocateShuttle)
        {
            try
            {
                var apiResponse = new ApiResponse();

                if (allocateShuttle.request_id>0 && allocateShuttle.vendor_id > 0)
                {
                    bool allocateFlag = await _manageTransportRequest.AllocateShuttle(allocateShuttle.request_id, allocateShuttle.vendor_id);
                    if (allocateFlag)
                    {
                        apiResponse.message = "Shuttle allocated successfully !!";
                        apiResponse.status = "Success";
                        return Ok(apiResponse);
                    }
                    else
                    {
                        apiResponse.message = "Something went wrong !!";
                        apiResponse.status = "Failed";
                        return BadRequest(apiResponse);
                    }
                }
                else
                {
                    apiResponse.message = "Request parameter invalid !!";
                    apiResponse.status = "Failed";
                    return BadRequest(apiResponse);
                }


            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        [ActionName("CancelledRequest")]
        public string CancelledRequest([FromBody]TransportRequest ApproveRequests)
        {
            return "Get 2";
        }
        

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return null;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TransportRequest requests)
        {

            return null;
        }
        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}