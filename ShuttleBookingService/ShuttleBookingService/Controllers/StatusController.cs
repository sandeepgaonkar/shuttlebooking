﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;

namespace ShuttleBookingService.Controllers
{
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class StatusController : ControllerBase
    {
        private readonly IStatusManager _statusManager;
        public StatusController(IStatusManager statusManager)
        {
            _statusManager = statusManager;
        }
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var statusDetails = await _statusManager.GetStatus();
                var apiResponse = new ApiResponse();
                if (statusDetails != null)
                {

                    apiResponse.message = "Find all status deatils";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(statusDetails);
                    return Ok(apiResponse);
                }
                else
                {
                    apiResponse.message = "Status deatils not found";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

        // GET: api/Status/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Status
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Status/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
