﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShuttleBookingService.Business.Repository;
using ShuttleBookingService.Model.Model;
using Microsoft.AspNetCore.Cors;
using System.ComponentModel;
using System.Dynamic;
using SendGrid;
using SendGrid.Helpers.Mail;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShuttleBookingService.Controllers
{
    [ApiController]
    [EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    public class TransportRequestController : ControllerBase
    {

        private readonly ITransportRequestManager _transportRequestManager;
        public TransportRequestController(ITransportRequestManager TransportRequestManager)
        {
            _transportRequestManager = TransportRequestManager;
        }
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var transportRequests = await _transportRequestManager.GetTransportRequests();
                
                var apiResponse = new ApiResponse();
                if (transportRequests != null)
                {
                    apiResponse.message = "Find all transport requests";
                    apiResponse.status = "Success";
                    foreach (var request in transportRequests)
                    {
                        apiResponse.data.Add(new ShuttleBookingResponse
                        {
                            request_id = request.request_id,
                            employee_id = request.employee_id,
                            employee_name = request.employee_name,
                            contact_phone = request.contact_phone,
                            account = request.account,
                            email = request.email,
                            request_reason = request.request_reason,
                            pickup_location_id = request.pickup_location_id,
                            pickup_location = request.pickup_location,
                            to_location = request.to_location,
                            pin_code = request.pin_code,
                            no_of_passengers = request.no_of_passengers,
                            vendor_id = request.vendor_id,
                            vendor_name = request.vendor_name,
                            vehicle_model = request.vehicle_model,
                            vehicle_number = request.vehicle_number,
                            total_expense = request.total_expense,
                            manager_id = request.manager_id,
                            manager_manager_id = request.manager_manager_id,
                            approved_by = request.approved_by,
                            approved_on = request.approved_on,
                            request_time = request.request_time,
                            request_date = request.request_date,
                            request_date_time = String.Format("{0} {1}", request.request_date.Date.ToString("dd-MM-yyyy"), DateTime.Today.Add(request.request_time).ToString("hh:mm tt")),
                            created_on = request.created_on,
                            status_id = request.status_id,
                            status_name = request.status_name,
                            cancelled_by = request.cancelled_by,
                            cancelled_on = request.cancelled_on,
                            comment = request.comment,
                        });
                    }
                    // SendEmail("sandeep","admin@emids.com", "" );
                    return Ok(apiResponse);
                }
                else
                {
                    apiResponse.message = "Find all transport requests";
                    apiResponse.status = "Success";
                    return NotFound(apiResponse);
                }



            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }
        // GET api/<controller>/5
        [HttpGet("{requestId}")]
        [Route("TransportRequestByReqId/{requestId}")]
        public async Task<IActionResult> Get(int requestId)
        {
            try
            {
                var request = await _transportRequestManager.GetTransportRequestById(requestId);
                var apiResponse = new ApiResponse();

                if (request == null)
                {
                    apiResponse.message = "Shuttle book request id is not valid !!";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                else
                {
                    apiResponse.message = "Shuttle booking request for request id '" + requestId + "'";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(new ShuttleBookingResponse
                    {
                        request_id = request.request_id,
                        employee_id = request.employee_id,
                        employee_name = request.employee_name,
                        contact_phone = request.contact_phone,
                        account = request.account,
                        email = request.email,
                        request_reason = request.request_reason,
                        pickup_location_id = request.pickup_location_id,
                        to_location = request.to_location,
                        pickup_location = request.pickup_location,
                        pin_code = request.pin_code,
                        no_of_passengers = request.no_of_passengers,
                        vendor_id = request.vendor_id,
                        total_expense = request.total_expense,
                        manager_id = request.manager_id,
                        manager_manager_id = request.manager_manager_id,
                        approved_by = request.approved_by,
                        approved_on = request.approved_on,
                        request_time = request.request_time,
                        request_date = request.request_date,
                        request_date_time = String.Format("{0} {1}", request.request_date.Date.ToString("dd-MM-yyyy"), DateTime.Today.Add(request.request_time).ToString("hh:mm tt")),
                        created_on = request.created_on,
                        status_id = request.status_id,
                        status_name = request.status_name,
                        cancelled_by = request.cancelled_by,
                        cancelled_on = request.cancelled_on,
                        comment = request.comment,
                    });
                    return Ok(apiResponse);

                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{empid}")]
        public async Task<IActionResult> Get(string empid)
        {
            try
            {
                var request = await _transportRequestManager.GetTransportRequestByEmpId(empid);
                var apiResponse = new ApiResponse();

                if (request == null)
                {
                    apiResponse.message = "Shuttle book emp id is not valid !!";
                    apiResponse.status = "Not Found";
                    return NotFound(apiResponse);
                }
                else
                {
                    apiResponse.message = "Shuttle booking request for emp id '" + empid + "'";
                    apiResponse.status = "Success";
                    apiResponse.data.Add(new ShuttleBookingResponse
                    {
                        request_id = request.request_id,
                        employee_id = request.employee_id,
                        employee_name = request.employee_name,
                        contact_phone = request.contact_phone,
                        account = request.account,
                        email = request.email,
                        request_reason = request.request_reason,
                        pickup_location_id = request.pickup_location_id,
                        to_location = request.to_location,
                        pickup_location = request.pickup_location,
                        pin_code = request.pin_code,
                        no_of_passengers = request.no_of_passengers,
                        vendor_id = request.vendor_id,
                        total_expense = request.total_expense,
                        manager_id = request.manager_id,
                        manager_manager_id = request.manager_manager_id,
                        approved_by = request.approved_by,
                        approved_on = request.approved_on,
                        request_time = request.request_time,
                        request_date = request.request_date,
                        request_date_time = String.Format("{0} {1}", request.request_date.Date.ToString("dd-MM-yyyy"), DateTime.Today.Add(request.request_time).ToString("hh:mm tt")),
                        created_on = request.created_on,
                        status_id = request.status_id,
                        status_name = request.status_name,
                        cancelled_by = request.cancelled_by,
                        cancelled_on = request.cancelled_on,
                        comment = request.comment,
                    });
                    return Ok(apiResponse);

                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPost]
        public async Task<IActionResult> CreateRequest([FromBody]TransportRequest requests)
        {
            try
            {
                var request = await _transportRequestManager.AddTransportRequest(requests);
                var apiResponse = new ApiResponse();
                apiResponse.message = "Shuttle book request is submitted successfully !!";
                apiResponse.status = "Success";

                apiResponse.data.Add(new ShuttleBookingResponse
                {
                    request_id = request.request_id,
                    employee_id = request.employee_id,
                    employee_name = request.employee_name,
                    contact_phone = request.contact_phone,
                    account = request.account,
                    email = request.email,
                    request_reason = request.request_reason,
                    pickup_location_id = request.pickup_location_id,
                    pickup_location = request.pickup_location,
                    to_location = request.to_location,
                    pin_code = request.pin_code,
                    no_of_passengers = request.no_of_passengers,
                    vendor_id = request.vendor_id,
                    total_expense = request.total_expense,
                    manager_id = request.manager_id,
                    manager_manager_id = request.manager_manager_id,
                    approved_by = request.approved_by,
                    approved_on = request.approved_on,
                    request_time = request.request_time,
                    request_date = request.request_date,
                    request_date_time = String.Format("{0} {1}", request.request_date.Date.ToString("dd-MM-yyyy"), DateTime.Today.Add(request.request_time).ToString("hh:mm tt")),
                    created_on = request.created_on,
                    status_id = request.status_id,
                    status_name = request.status_name,
                    cancelled_by = request.cancelled_by,
                    cancelled_on = request.cancelled_on,
                    comment = request.comment,
                });
                SendEmail(requests.employee_name, "admin@emids.com", requests.email);
                return Ok(apiResponse);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error " + ex.Message);
            }

        }
        public void SendEmail(string requestername, string requesteremailid, string approveremailid)
        {
            try
            {
                var apiKey = "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";
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress("sandeep.gaonkar@live.com", "Admin User");
                var subject = "Cab Request raised by " + requestername;
                var to = new EmailAddress(approveremailid, "Example User");
                var plainTextContent = "Cab request has been raised by  " + requestername + " Please approve.";
                var htmlContent = "<strong>Cab request has been raised by  " + requestername + " Please approve." + "</strong>";
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = client.SendEmailAsync(msg);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
