using ShuttleBookingService.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService
{
  
    public class ApiResponse
    {
        public string message { get; set; }
        public string status { get; set; }

        public List<object> data = new List<object>();
        //public List<ShuttleBookingResponse> dscRequestDetails = new List<ShuttleBookingResponse>();
    }

    public class GenerateReport
    {
        public DateTime fromDate;
        public DateTime toDate;
        public bool exportFlag;
    }

    public class ShuttleBookingResponse
    {
        public int request_id { get; set; }
        public string employee_id { get; set; }
        public string contact_phone { get; set; }
        public string account { get; set; }
        public string email { get; set; }
        public string request_reason { get; set; }
        public int pickup_location_id { get; set; }
        public string pickup_location { get; set; }
        public string to_location { get; set; }
        public string pin_code { get; set; }
        public int no_of_passengers { get; set; }
        public int vendor_id { get; set; }
        public string vendor_name { get; set; }
        public string vehicle_number { get; set; }
        public string vehicle_model { get; set; }
        public string total_expense{ get; set; }
    public string manager_id { get; set; }
        public string manager_manager_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public TimeSpan request_time { get; set; }
        public DateTime request_date { get; set; }
        public string request_date_time { get; set; }
        public DateTime created_on { get; set; }
        public int status_id { get; set; }
        public string status_name { get; set; }
        public string cancelled_by { get; set; }
        public DateTime? cancelled_on { get; set; }
        public string comment { get; set; }
        public char gender { get; set; }
        public string employee_name { get; set; }
    }
}
