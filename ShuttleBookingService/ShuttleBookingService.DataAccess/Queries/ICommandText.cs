﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.DataAccess.Queries
{
    public interface ICommandText
    {
        string GetAllTransportRequests { get;}

        string GetTransportRequestById { get; }

        string GetTransportRequestByempId { get; }
        string GetTransportRequestByManagerId { get; }
        string GetAllGetPickupLocations { get; }
        string GetAllVendors{ get; }
        string VendorsRegistration { get; }
        string GetVendorById { get; }
        string UpdateVendorById { get; }
        string DeleteVendorById { get; }

        string GetAllStatus { get; }
        string AddTransportRequest { get; }
        string AddEmployeeDetails { get; }
        string ApproveTransportRequest { get; }
        string ApproveByManager { get; }
        // string ApproveByManagerManager { get; }
        string CancelledByManager { get; }
        
        string AllocateShuttle { get; }

        string GenerateReportByDate { get; }


    }
}
