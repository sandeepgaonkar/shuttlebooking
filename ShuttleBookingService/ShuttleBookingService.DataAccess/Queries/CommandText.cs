﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.DataAccess.Queries
{
    public class CommandText : ICommandText
    {
       
        //TODO commented get records for today
        //public string GetAllTransportRequests => " select *,st.status_name,lp.location as pickup_location from public.transport_request" +
         //   " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where  DATE(tr.created_on) = now()::date";
        public string GetAllTransportRequests1 => "select vm.vendor_name ,vm.vehicle_number,vm.vehicle_model,*,st.status_name,lp.location as pickup_location from public.transport_request " +
            " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id left join vendor_master vm " +
            " on tr.vendor_id = vm.vendor_id  where  DATE(tr.created_on) = now()::date";

        public string GetAllTransportRequests => "select vm.vendor_name ,vm.vehicle_number,vm.vehicle_model,*,st.status_name,lp.location as pickup_location from public.transport_request " +
            " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id left join vendor_master vm " +
            " on tr.vendor_id = vm.vendor_id ";


        public string GetTransportRequestById => "select *,st.status_name,lp.location as pickup_location from public.transport_request" +
            " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where  tr.request_id = @request_id";
        public string GetTransportRequestByempId => "select *,st.status_name,lp.location as pickup_location from public.transport_request" +
           " tr join public.status st on tr.status_id= st.status_id join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id where  tr.employee_id = @request_id order by request_id desc limit 1";

        public string GetTransportRequestByManagerId => "select *,st.status_name,emp.employee_name, emp.gender, lp.location as pickup_location from public.transport_request tr join " +
                 "   public.status st on tr.status_id= st.status_id join library_pickup_location lp on  " +
                  "  tr.pickup_location_id = lp.pickup_location_id  " +
                   " join public.employee emp on emp.employee_id = tr.employee_id where  " +
                   " DATE(tr.created_on) = now()::date and (tr.manager_id = @managerManagerId or tr.manager_manager_id = @managerManagerId)";

        public string AddTransportRequest => "INSERT INTO public.\"transport_request\"(employee_id, contact_phone, account, email, request_reason, pickup_location_id, to_location, to_lattitude, to_longitude, pin_code, no_of_passengers, manager_id, manager_manager_id, request_time, request_date, created_on, status_id)" +
            "VALUES(@employee_id, @contact_phone, @account, @email, @request_reason, @pickup_location_id, @to_location, @to_lattitude, @to_longitude, @pin_code, @no_of_passengers, @manager_id, @manager_manager_id, @request_time, @request_date,NOW(),1)"+
            "RETURNING request_id";

        public string AddEmployeeDetails => "UPDATE public.\"employee\" SET phone_no= @phone_no , emergency_contact= @emergency_contact, email= @email, " +
            "account= @account, project= @project,practice_area= @practice_area, manager_id= @manager_id, manager_name= @manager_name, manager_username= @manager_username," +
            "manager_email= @manager_email, manager_phone_no= @manager_phone_no, manager_emergency_contact= @manager_emergency_contact," +
            "manager_manager_id= @manager_manager_id, manager_manager_name = @manager_manager_name, manager_manager_username= @manager_manager_username, " +
            "manager_manager_email= @manager_manager_email, manager_manager_phone_no= @manager_manager_phone_no, manager_manager_emergency_contact= @manager_manager_emergency_contact, modified_on= NOW() " +
            "where employee_id = @employee_id ;" +
            "INSERT INTO public.\"employee\"(employee_id, employee_name, employee_username, gender, phone_no, emergency_contact, email, account, " +
            "project, practice_area, manager_id, manager_name, manager_username, manager_email, manager_phone_no, manager_emergency_contact, manager_manager_id, " +
            "manager_manager_name, manager_manager_username, manager_manager_email, manager_manager_phone_no, manager_manager_emergency_contact, " +
            "created_on,active) SELECT " +
            "@employee_id,@employee_name, @employee_username, @gender, @phone_no, @emergency_contact, @email, @account, @project," +
            "@practice_area, @manager_id, @manager_name, @manager_username, @manager_email, @manager_phone_no, @manager_emergency_contact," +
            "@manager_manager_id, @manager_manager_name, @manager_manager_username, @manager_manager_email, @manager_manager_phone_no," +
            "@manager_manager_emergency_contact, NOW(), '1' WHERE NOT EXISTS(SELECT 1 FROM public.\"employee\" WHERE employee_id=@employee_id)";

        public string GetAllGetPickupLocations => "select * from public.\"library_pickup_location\"";
        

        public string GetAllStatus => "select * from public.\"status\"";
        public string ApproveTransportRequest => " update transport_request set status_id=2, approved_by ='Auto Approved', comment ='Auto Approved' where "+ 
           " (DATE_PART('day', current_timestamp - created_on) * 24 + DATE_PART('hour', current_timestamp - created_on)) * 60 + " +
           "    DATE_PART('minute', current_timestamp - created_on)>30 and status_id=1";
        public string ApproveByManager => "update public.transport_request  set " +
           "status_id = @satusid, approved_by = @approved_by, approved_on = @approved_on," +
           "comment= @comment where request_id = @request_id";

        public string CancelledByManager => "update public.transport_request  set " +
           "status_id = @satusid, cancelled_by = @cancelled_by, cancelled_on = @cancelled_on," +
           "comment= @comment where request_id = @request_id";
        /*public string ApproveByManagerManager => "update public.\"transport_request\"  set " +
           "approved_by = (select manager_manager_name from public.\"employee\" where employee_id = @employee_id)," +
           "approved_on= NOW() where request_id = @request_id";*/
        public string AllocateShuttle => "update public.\"transport_request\"  set vendor_id = @vendorId::int4 where request_id = @requestId";

        public string GetAllVendors => "select * from public.\"vendor_master\" where status = '1' ";
        public string VendorsRegistration => "INSERT INTO public.\"vendor_master\"(vehicle_id, vendor_name, vendor_contact, vendor_address, vendor_email," +
            " vehicle_number, vehicle_model, owner_name, owner_contact, driver_name, driver_contact, license_number, insurance_valid, insurance_valid_till," +
            " status, no_of_seats) " +
            "VALUES(@vehicle_id, @vendor_name, @vendor_contact, @vendor_address, @vendor_email, " +
            "@vehicle_number, @vehicle_model, @owner_name, @owner_contact, @driver_name, @driver_contact," +
            "@license_number, @insurance_valid, @insurance_valid_till, @status, @no_of_seats)" +
            " RETURNING vendor_id, vehicle_id, vendor_name, vendor_contact, vendor_address, vendor_email," +
            " vehicle_number, vehicle_model, owner_name, owner_contact, driver_name, driver_contact, " +
            "license_number, insurance_valid, insurance_valid_till, status, no_of_seats";
        public string GetVendorById => "select * from public.\"vendor_master\" where vendor_id = @vendor_id";

         public string DeleteVendorById => "update public.\"vendor_master\" set status='0' where vendor_id = @vendor_id RETURNING vendor_id,status";

        public string UpdateVendorById => "update public.\"vendor_master\" set vehicle_id=@vehicle_id, vendor_name=@vendor_name, " +
            "vendor_contact=@vendor_contact, vendor_address=@vendor_address, vendor_email=@vendor_email, vehicle_number=@vehicle_number, " +
            "vehicle_model=@vehicle_model, owner_name=@owner_name, owner_contact=@owner_contact, driver_name=@driver_name, driver_contact=@driver_contact," +
            " license_number=@license_number, insurance_valid=@insurance_valid, insurance_valid_till=@insurance_valid_till, " +
            "status=@status, no_of_seats=@no_of_seats where vendor_id= @vendor_id " +
            "RETURNING vendor_id, vehicle_id, vendor_name, vendor_contact, vendor_address, vendor_email," +
            "vehicle_number, vehicle_model, owner_name, owner_contact, driver_name, driver_contact, " +
            "license_number, insurance_valid, insurance_valid_till, status, no_of_seats";


        public string GenerateReportByDate => "select tr.request_id,tr.employee_id, " +
            "tr.contact_phone,tr.account,tr.email,tr.status_id,tr.request_reason,emp.gender,emp.employee_name, " +
            "tr.pickup_location_id,lp.location as pickup_location,tr.to_location , tr.no_of_passengers ," +
             "  tr.created_on, vm.vendor_name ,vm.vehicle_number,vm.vehicle_model,st.status_name,lp.location as pickup_location from " +
           " public.transport_request tr join public.status st on tr.status_id= st.status_id " +
          "  join library_pickup_location lp on tr.pickup_location_id = lp.pickup_location_id " +
          "  left join vendor_master vm on tr.vendor_id = vm.vendor_id  join employee  emp on emp.employee_id = tr.employee_id " +
            "  where DATE(tr.created_on) in (@fromDate::date,@toDate::date)";

    }
}
