﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Model.Model
{
    public class Vendor
    {
        public int vendor_id { get; set; }
        public int vehicle_id { get; set; }
        public string vendor_name { get; set; }
        public string vendor_contact { get; set; }
        public string vendor_address { get; set; }
        public string vendor_email { get; set; }
        public string vehicle_number { get; set; }
        public string vehicle_model { get; set; }
        public string owner_name { get; set; }
        public string owner_contact { get; set; }
        public string driver_name { get; set; }
        public string driver_contact { get; set; }
        public string license_number { get; set; }
        public string insurance_valid { get; set; }

        [DataType(DataType.Date)]
        public DateTime? insurance_valid_till { get; set; }
        public string status { get; set; }
        public int no_of_seats { get; set; }
    }
}
