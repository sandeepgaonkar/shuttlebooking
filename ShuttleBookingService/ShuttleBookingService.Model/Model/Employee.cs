﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Model.Model
{
    public class Employee
    {
        [Required]
        public string employee_id { get; set; }
        [Required]
        public string employee_name { get; set; }
        public string employee_username { get; set; }
        public char gender { get; set; }
        public string phone_no { get; set; }
        public string emergency_contact { get; set; }
        [Required]
        public string email { get; set; }
        public string account { get; set; }
        public string project { get; set; }
        public string practice_area { get; set; }
        public string manager_id { get; set; }
        public string manager_name { get; set; }
        public string manager_username { get; set; }
        public string manager_email { get; set; }
        public string manager_phone_no { get; set; }
        public string manage_emergency_contact { get; set; }
        public string manager_manager_id { get; set; }
        public string manager_manager_name { get; set; }
        public string manager_manager_username { get; set; }
        public string manager_manager_email { get; set; }
        public string manager_manager_phone_no { get; set; }
        public string manager_manager_emergency_contact { get; set; }
        public DateTime? created_on { get; set; }
        public DateTime? modified_on { get; set; }
        public bool active { get; set; }
    }
}
