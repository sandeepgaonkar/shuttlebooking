﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShuttleBookingService.Model.Model
{
    public class TransportRequest
    {
        public int request_id { get; set; }
        [Required]
        public string employee_id { get; set; }
        [Required]
        public string contact_phone { get; set; }
        public string account { get; set; }
        [Required]
        public string email { get; set; }
        public string request_reason { get; set; }
        [Required]
        public int pickup_location_id { get; set; }
        public string pickup_location { get; set; }
        [Required]
        public string to_location { get; set; }
        public string to_lattitude { get; set; }
        public string to_longitude { get; set; }
        public string pin_code { get; set; }
        public int no_of_passengers { get; set; }
        public int vendor_id { get; set; }
        public string vendor_name { get; set; }
        public string vehicle_number { get; set; }

        public string vehicle_model { get; set; }
        public string total_expense { get; set; }
        public string manager_id { get; set; }
        public string manager_manager_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public TimeSpan request_time { get; set; }
        public DateTime request_date { get; set; }
        public DateTime created_on { get; set; }
        public int status_id { get; set; }
        public string status_name { get; set; }
        public string cancelled_by { get; set; }
        public DateTime? cancelled_on { get; set; }
        public string comment { get; set; }
        [Required]
        public string employee_name { get; set; }
        public string employee_username { get; set; }
        public char gender { get; set; }
        public string phone_no { get; set; }
        public string emergency_contact { get; set; }
        public string project { get; set; }
        public string project_account { get; set; }
        public string practice_area { get; set; }
        public string manager_name { get; set; }
        public string manager_username { get; set; }
        public string manager_email { get; set; }
        public string manager_phone_no { get; set; }
        public string manager_emergency_contact { get; set; }
        public string manager_manager_name { get; set; }
        public string manager_manager_username { get; set; }
        public string manager_manager_email { get; set; }
        public string manager_manager_phone_no { get; set; }
        public string manager_manager_emergency_contact { get; set; }
        public DateTime? modified_on { get; set; }
        public bool active { get; set; }

    }
}

   
